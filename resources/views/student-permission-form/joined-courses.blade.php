@extends('adminlte::page')
@section('title', 'Permission Form')
@section('content_header')
<?php

  $date = Carbon\Carbon::now();
  $date = date_format($date,'d M Y');

  /*
  $i = 0;
  if(empty($regno)){
    ?>
    <h1 style="text-align: center;" target_blank><b>No Registration Number Found, Kindly Visit Automation Center</b></h1>
    <?php
  }else{
  $currentSemester = '';
  foreach($semcode as $key=>$year)
    if ($i == 0) {
      $currentSemester = $year->semcode;
      break;
    }
     $coursecodes=array();
     */
?>
  {{-- <a href="/semester-result-intimation" style="    float: right;" target="_blank" class="btn btn-primary btn-sm" role="button">Download Result Intimation</a> --}}
  <div class="current_date"><strong>Date: <?php echo $date ?></strong></div>

<h1 class="headingOne" style="text-align: center;font-size: 40px;"><b>INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</b></h1>
  <h1 style="text-align: center;font-size: 35px;text-transform: uppercase;font-weight: 900;"><b>Result Intimation</b></h1>
  <!-- <h1 style="text-align: center;"><b>(Course Registration / Permission Form For <?php /* echo $currentSemester */ ?>)</b></h1> -->
  {{-- <h1 style="text-align: center;font-size: 25px;"><b>SEMESTER SPR-2023</b></h1> --}}

{{-- Dev Mannan: Code Starts --}}
<div>

    <form action="{{ route('downloadResultIntimationPDF') }}" method="POST" class="form-inline">
        @csrf
        <label class="mb-2 mr-sm-2" for="semester">Semester:</label>

        <select class="form-control mb-2 mr-sm-2" name="semester" id="semester" required>
            <option value="">Select Semester</option>
            <option value="FALL">FALL</option>
            <option value="SPR">SPR</option>
            <option value="SUM">SUM</option>
        </select>

        <label class="mb-2 mr-sm-2" for="year">Year:</label>
        <select class="form-control mb-2 mr-sm-2" name="year" id="year" required>
            <option value="">Select Year</option>

            <?php

            for ($i = date("Y"); $i >= 2000; $i--){

                echo '<option value="'.$i.'">'.$i.'</option>';

            }
            ?>
        </select>


        <button type="submit" class="btn btn-primary mb-2">Download Result Intimation</button>
    </form>


</div>


{{-- Dev Mannan: Code Ends --}}
<?php /* ?>
  <table class="table table-hover" id="student-data" style="width:80%; margin-top: 20px;">
    <tbody>
        <tr>
            <td><strong>Registration No</strong></td>
          <td><?php
          if(!empty($year->regno)){
          echo $year->regno;
          }
          ?></td>
        </tr>
         <tr>
          <td><strong>Student Name:</strong></td>
          <td>{{ $year->studname }}</td>
        </tr>
         <tr>
          <td><strong>Father's Name:</strong></td>
          <td>{{ $year->studfathername }}</td>
        </tr>
        <tr>
          <td><strong>Faculty:</strong></td>
          <td>{{ $year->facname }}</td>
        </tr>
        <tr>
          <td><strong>Department:</strong></td>
          <td>{{ $year->depname }}</td>
        </tr>

        <tr>
          <td><strong>Dgree Programme</strong></td>
          <td>{{ $year->acadprogname }}</td>
          <!--
            <td><strong>Phone #</strong></td>
            <td>{{ $year->studphone }}</td>
          -->
        </tr>
         <!--
          <tr>
            <td><strong>Email:</strong></td>
            <td>{{ $year->studemail }}</td>
          </tr>
        -->
        </tr>
    </tbody>
  </table>
          <?php
            $currentSemester='';
          ?>
            @foreach($semcode as $key=>$year)
            <?php
            if(in_array($year->coursecode,$coursecodes)){
            continue;
            }
            $coursecodes[]=$year->coursecode;
                if($year->semcode!='SPR-2022'){
                    continue;
                }
            ?>
               @if(empty($currentSemester))
                <?php
                $currentSemester=$year->semcode;
              ?>
              <table class="table table-hover" style="width:80%">
                <thead class="thead-dark">
                  <tr>
                      <th style="width: 12%;">Course Code</th>
                      <th style="width: 35%;">Course Title</th>
                      <th style="width: 12%;">Cr. Hrs</th>
                      <th style="width: 12%;">Grade</th>
                  </tr>
                </thead>
                <tbody>
                     @elseif($currentSemester!=$year->semcode)
                      <?php
                          $currentSemester=$year->semcode;
                       ?>
                </tbody>
              </table>
              <div><strong>Semester </strong> {{ $year->semcode }}</div>
                 <table class="table table-hover" style="width:80%">
                    <thead class="thead-dark">
                      <tr>
                          <th style="width: 12%;">Code</th>
                          <th style="width: 35%;">Course Title</th>
                          <th style="width: 12%;">Cr. Hrs</th>
                          <th style="width: 12%;">Grade</th>

                      </tr>
                  </thead>
                  <tbody>
                         @endif
                        <tr>
                          <td>{{ $year->coursecode }}</td>
                          <td>{{ $year->coursename }}</td>
                          <td>{{ $year->credithrs }}</td>
                          <td>
                            @if($year->semcode!='FALL-2021' )
                          {{ $year->grade }}
                        @endif
                        </td>
                        </tr>
                      @endforeach
                      <?php
  }
  ?>
                      <!-- <tr>
                        <td><b>GPA:</b>gpa val</td>
                        <td><b>Total Cr.:</b>credit hour val</td>
                        <td><b>Cm%age:</b>%age val</td>
                      </tr>
                      <tr>
                        <td><b>Credit Attempted:</b>total cr hr val</td>
                        <td><b>Credit Pass:</b>pass cr hr val</td>
                      </tr> -->
                  </tbody>
              </table>
              <table id="cgpa">
                  <tr>
                      <td style="font-size: 20px;font-weight: bold;">Cummulative GPA:  <span style="font-weight: normal;border-bottom: 1px solid #000;">{{ substr($year->cgpa,0,4) }}/4.00</span></td>
                  </tr>
              </table>
              <style>
                  table#student-data tr td:first-child {
    max-width: 50px !important;
}
              </style>


<?php */ ?>


@stop
@section('content')

@endsection
