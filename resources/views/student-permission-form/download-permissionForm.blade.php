<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Permission Form</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{public_path('permission-form/download-permission-form.css')}}" rel="stylesheet" type="text/css" media="all">
<style>
  @media print {
	  @page { size: [portrait]; }
}
.container {
	/* width: 100%; */
	margin: 0 auto;
	font-family: 'Open Sans', sans-serif;
}
.date-div{
  text-align: center;
  float:right;
}
.headingOne{
  clear: both;
  text-align: center;
  font-size:15px;
}
.headingTwo{
  text-align: center;
  font-size:14px;
  text-decoration: underline;
}
.no-border-table{
  width:100%;
}
.no-border-table td{
  border: 0px;
  font-size:16px;
}
.no-border-table th{
  border: 0px;
  font-size:16px;
}
.no-border-table strong{
  border: 0px;
  font-size:16px;
}
.smooth-border{
  border:2px solid #000000;
  margin-top:5px;
}
.perm-tab th{
  border: 1px solid #333;
  text-align: left;
  padding: 2px;
}
.perm-tab td{
  border: 1px solid #333;
  text-align: left;
  padding: 2px;
}
table td{
  border: 0px;
  padding: 2px;
}
table {
  border-collapse: collapse;
  align:"center"
}
th, td, strong {
  font-size: 10px;
}
div, strong {
  font-size: 10px;
}
</style>
</head>
    @php
  $date = Carbon\Carbon::now();
  $date = date_format($date,'d M Y');

  $i = 0;
  if(!empty($regno)){
@endphp
    <h1><b>No Registration Number Found, Kindly Visit Automation Center</b></h1>
@php
  }else{
  $currentSemester = '';
  $semester_courses=array();
  foreach($semcode as $key=>$year){
    $semester_courses[$year->semcode][]=$year;
    if(empty($currentSemester)){
      $currentSemester=$year->semcode;
    }
  }
  $currentcourses=false;
  if(!empty($semester_courses['SPR-2024'])){
    $currentcourses=$semester_courses['SPR-2024'];
    unset($semester_courses['SPR-2024']);
  }
@endphp

  <h1 class="headingOne"><b>INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</b><div class="date-div" style="float:right"><strong>Date: @php echo $date @endphp</strong></div></h1>
  <h1 class="headingTwo"><b>(Course Registration / Permission Form For @php  echo $currentSemester @endphp)</b></h1>
  <!-- Top Table Start -->
  <table class="no-border-table">
    <tbody>
        <tr>
          <td><strong>Registration Number</strong> {{ $year->regno }}</td>
          <td><strong>Faculty:</strong> {{ $year->facname }}</td>
        </tr>
        <tr>
          <td><strong>Student Name:</strong> {{ $year->studname }} </td>
          <td><strong>Department:</strong> {{ $year->depname }}</td>
        </tr>
        <tr>
          <td><strong>Father Name:</strong> {{ $year->studfathername }}</td>
          <td><strong>Batch:</strong> {{ $year->batchname }}</td>
        </tr>
        <tr>
          <td><strong>Programme: </strong> {{ $year->acadprogname }}</td>
          <td><strong>Phone #</strong> {{ $year->studphone }}</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Email:</strong> {{ $year->studemail }}</td>
        </tr>
    </tbody>
  </table>
  <!-- Top Table End -->
  <div class="smooth-border"></div>
  <div><strong>Semester Results </strong></div>
          @php
           $index=0;
          @endphp
         <div style="width:50%;float:left;padding-right:10px;border-right:2px solid #000;display:inline-block">
            @foreach($semester_courses as $key=>$semester_course)
              <div><strong>Semester </strong> {{ $key }}</div>
                <table class="perm-tab" style="width:100%;">
                  <thead>
                    <tr>
                        <th style="width:10%;">Code</th>
                        <th style="width: 25%;">Course Title</th>
                        <th style="width:10%;">Grd</th>
                        <!-- <th>Credit Hours</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($semester_course as $skey=>$sem_course)
                    <tr>
                      <td>{{ $sem_course->coursecode }}</td>
                      <td>{{ $sem_course->coursename }}</td>
                      <td>{{ $sem_course->grade }}</td>
                      <!-- <td>{{ $sem_course->credithrs }}</td> -->
                    </tr>
                    @endforeach
                    <!-- <tr>
                      <td><strong>CGPA:</strong>cgpa val</td>
                      <td><strong>Total Cr.:</strong>total cr. val</td>
                      <td><strong>Cm%age:</strong>cm%age val</td>
                      <td><strong>Degree:</strong>degree val</td>
                    </tr> -->
                  </tbody>
                </table>
              @php
              $index++;
              @endphp
            @endforeach
</div>
          <!-- 1st TD end -->
          <!-- 2nd TD start -->
          <div style="vertical-align: top;width:47%;float:right;padding-left:10px;clear:both">
          @if($currentcourses)
            <div><strong>Semester FALL-2021</strong></div>
            <table style="width:100%;">
              <thead>
                <tr>
                    <th style="width:50px; text-align: left;">Code</th>
                    <th style="text-align: left;">Course Title</th>
                    <th style="width:30px; text-align: left;">Type</th>
                    <th style="width:30px; text-align: left;">C.H</th>
                </tr>
              </thead>
              <tbody>
            @foreach($currentcourses as $key=>$semester_course)
            <tr>
              <td>{{ $semester_course->coursecode }}</td>
              <td>{{ $semester_course->coursename }}</td>
              <td>CR</td>
              <td>{{ $semester_course->credithrs }}</td>
            </tr>
            @endforeach
            </tbody>
            </table>
            @endif
            <div style="border-top:2px solid #000000; padding-top:5px;float:right;width:40%;margin-top:50px;text-align:center;font-weight:bold;font-size:13px">
            Student's Signature
            </div>
            <div style="border-top:2px solid #000000; padding-top:5px;width:40%;margin-top:50px;text-align:center;font-weight:bold;font-size:13px">
            HOD/Director/Dy.Dean's Signature
            </div>

            <table width="100%" style="margin-top:100px;">
              <tbody>
                <tr>
                  <td colspan="2" style="text-decoration: underline;">For Official Use</td>
                </tr>
                <tr>
                  <td> > Original documents checked and found correct.</td>
                  <td><div style="border-top:2px solid #000000; padding-top:2px;width:70%;text-align:center;font-weight:bold;font-size:10px">
                  Academic Advisor
                  </div></td>
                </tr>
                <tr>
                  <td colspan="2"> > Required fee paid. Pleaase process further.</td>
                </tr>
                <tr>
                  <td style="text-decoration: underline;">Automation Centre</td>
                  <td><div style="border-top:2px solid #000000; padding-top:2px;width:70%;text-align:center;font-weight:bold;font-size:10px">
                  Accounts Office
                  </div></td>
                </tr>
              </tbody>
            </table>
</div>
          <!-- 2nd TD end -->

@php
}
@endphp

</body>
</html>
@php
 // exit;
@endphp
