@extends('adminlte::page')

@section('title', 'SIS | Exam Roll No Slips')


@section('content_header')
    <h1>Roll No Slip <small>(<strong>{{ $rollnoslip[0]->semcode }}</strong>)</small></h1>
@stop

@section('content')
    @if (count($rollnoslip))

    <?php 
    $feedback = 'given';
    $feedbackcount=0;
    /*
    foreach ($rollnoslip as $slips){

        if((int)$slips->feedback != 1 && stripos($slips->coursename, 'research project') === false && stripos($slips->coursename, 'internship') === false){
            $feedback = 'not-given';
            $feedbackcount++;
        }
    } */
    if($feedback == 'not-given'){
        echo 'Roll Number slip cannot be visible until you finished all course feedback on <a href="https://dashboard.iiu.edu.pk/">LMS</a>';
        ?>
         <h5 class="d-flex justify-content-center"><u>DETAIL OF COURSES, ALLOWED TO BE APPEARED IN
                                        EXAM {{ $rollnoslip[0]->semcode }} {{-- SEMESTER --}}</u></h5>
        <div class="col-md-8">
                                <div class="card">
                                    <div class="card-body p-0">
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px">S. No</th>
                                                    <th>Course Code</th>
                                                    <th>Course Title</th>
                                                    <th>Feedback Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $sn = 0; ?>
                                                @foreach ($rollnoslip as $slips)
                                                    <?php $sn++; ?>
                                                    <tr>
                                                        <td><?php echo $sn; ?></td>
                                                        <td>{{ $slips->coursecode }}</td>
                                                        <td>{{ $slips->coursename }}</td>
                                                        <td>@if((int)$slips->feedback == 1) <span style="color:green">Feedback Provided</span> @else <span style="color:red">Missing the Feedback</span> @endif
                                                        </td>
                                                        <!-- <td>_________________ </td> -->
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-info">To download your roll number slip, you must provide feedback for all courses on the <a href="https://dashboard.iiu.edu.pk/">LMS</a>. If any of your courses are missing on the LMS, please contact the respective course teacher to submit your midterm result.</div>
        <?php
    }else{
    ?>

        <div class="row no-print">
            <div class="col-12">
                {{-- <a onClick="window.print()" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a> --}}
                <a href="{{ route('downloadExamSlip') }}" target="_blank" class="btn btn-primary float-right"
                    style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                </a>
            </div>
        </div>


        <hr>
        <?php if(!empty($picture)){
				$blobimg = $picture;
		  ?>
        <div class="content"
            style="background:url('https://i.ibb.co/dQgj9sn/rollslip-bg.png');background-size: contain;background-repeat: no-repeat;background-position: center center;">
            <div class="container-fluid">
                <div class="card card-default">
                    <div class="card-header">
                        <div class="col-md-3 float-right">
                            <?php echo '<img src ="data:image/jpeg;base64,'.base64_encode($blobimg).'" id="pic1" src="#" alt="your image" class="img-thumb" width=105 height=105 border=1/>'; }?>
                            {{-- <img src="data:image/png;base64,{{ $decodedImage }}" alt="QR Code"> --}}
                        </div>
                        <h5 class="d-flex justify-content-center"><u>INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</u></h5>
                        <!-- <h6 class="d-flex justify-content-center"><u>DIRECTORATE OF ACADEMICS & EXAMINATIONS</u></h6>   -->
                        <h6 class="d-flex justify-content-center"><u>(EXAMINATION BRANCH)</u></h6>
                         <h6 class="d-flex justify-content-center"><u>ROLL NO SLIP FOR MID TERM EXAMINATION</h6>  
                         {{-- <h6 class="d-flex justify-content-center"><u>ROLL NO SLIP FOR TERMINAL EXAMINATION</h6> --}}
                        
                        <h6 class="d-flex justify-content-center"><u>(<b>{{ $rollnoslip[0]->semcode }}</b>) SEMESTER </h6>
                        </u>
                        </h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <strong>
                                <u>Roll No. / Reg No#</u></b>&nbsp; <u>{{ $rollnoslip[0]->regno }}</u>
                            </strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <p>Student Name Mr./Ms:</p> &nbsp;_<p style="text-decoration:underline;">
                                    <b>{{ $rollnoslip[0]->studname }}</b>______________________________________________________
                                </p></span>
                            </div>
                            <div class="row">
                                <p>Son/Daughter of:</p> &nbsp;_<p style="text-decoration:underline;">
                                    <b>{{ $rollnoslip[0]->studfathername }}</b>____________________________________________________
                                </p></span>
                            </div>
                            <div class="row">
                                <p>Faculty/Institute/School:</p> &nbsp;_<p style="text-decoration:underline;">
                                    <b>{{ $faculty[0]->facname }}</b>_______________________________</p></span>
                            </div>
                            <div class="row">
                                <p>Degree Program:</p> &nbsp;_<p style="text-decoration:underline;">
                                    <b>{{ $acadprog[0]->acadprogname }}</b>_________________<b>{{ $batch[0]->batchname }}</b>___
                                </p></span>
                            </div>
                            <div class="row">
                                <p>Examination to be held in:</p> &nbsp;_<p style="text-decoration:underline;">
                                    <b>{{ 'Nov-2024' }}{{-- date('M-Y', strtotime($semdate[0]->semenddate)) --}}</b>________________________________________________
                                </p></span>
                            </div>
                            <br>
                            <div class="justify-content" style="margin-right:25%">
                                <h5 class="d-flex justify-content-center"><u>DETAIL OF COURSES, ALLOWED TO BE APPEARED IN
                                        EXAM {{ $rollnoslip[0]->semcode }} {{-- SEMESTER --}}</u></h5>
                            </div>
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-body p-0">
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px">S. No</th>
                                                    <th>Course Code</th>
                                                    <th>Course Title</th>
                                                    <th>Remarks</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $sn = 0; ?>
                                                @foreach ($rollnoslip as $slips)
                                                    <?php $sn++; ?>
                                                    <tr>
                                                        <td><?php echo $sn; ?></td>
                                                        <td>{{ $slips->coursecode }}</td>
                                                        <td>{{ $slips->coursename }}</td>
                                                        <td>{{ strtolower($slips->crspreventstatus) == 'p' ? 'Prevented' : '' }}
                                                        </td>
                                                        <!-- <td>_________________ </td> -->
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <!--<div class="col-md-3 float-right">
              <span>______________________</span>
              <p>&nbsp;(Addl | Asst. /Dy Director)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Examination)</p>
              </div>
              <div class="col-md-3">
              <span>____________________</span>
              <p>&nbsp;&nbsp;(Dealing Assistant Exam)</p>
              </div> -->
                            <br><br>
                            <div class="col-md-12">
                                <b><u>Note:</u></b>
                                <p>
                                <ol type="i">
                                    <li>Bring this slip and your University Identity Card in the Examination Hall</li>
                                    <li>Possession of a Cell Phone/Digital Gagets in the Examination Hall is an offence as
                                        per University code of conduct.</li>
                                    <!-- <li>Your Registration Number will be considered as Roll No.</li> -->
                                    <li>All Roll No Slips are issued provisionally; therefore conduct of examination does
                                        not confer any right to a student to claim result of these courses.</li>
                                    <li>Students should not appear in the exams if having short attendance (less than 75%)
                                        in any course(s) and result shall be cancelled in case they appear in the
                                        examination.</li>
                                    @if ($defaultstud)
                                        <li>You are provisionally permitted to appear in the Mid Term Examinations Fall
                                            2023; however be reminded/informed that:-.</li><br>
                                        <b style="font-size:13px">&nbsp;&nbsp;&nbsp;&nbsp;i). Your result for Semester Fall
                                            2023 shall be withheld and you cannot claim for the results, based on this
                                            conditional permission.
                                        </b><br>
                                        <b style="font-size:13px">&nbsp;&nbsp;&nbsp;&nbsp;ii). Course registration for the
                                            next semester (Spring 21) shall not be allowed without clearance of fee default.
                                            Moreover, you shall not be allowed to attend classes without submission of
                                            outstanding fee.
                                        </b><br>
                                        <b style="font-size:13px">&nbsp;&nbsp;&nbsp; iii). You are also advised to clear/pay
                                            outstanding dues/fee at earliest to avoid any issues in the future.
                                        </b>
                                    @endif
                                </ol>
                                </p>
                                <br>
                                <i style="font-size:10px">Al-Jamia (IT-Center) Auto Generated on <?php echo date('M-d-Y H:m:i A'); ?></i>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php 
    }
    ?>
    @else
        <strong class="text-danger">No Roll No Slip against your record, Please contact <a
                href="https://support.iiu.edu.pk/" target="_blank">LMS Support.</strong></a>
    @endif
@stop
