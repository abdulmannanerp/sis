<!-- https://imgbb.com/ -->
<!-- https://www6.lunapic.com/editor/ -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Roll No Slip</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{public_path('challan/download-rollno-slip.css')}}" rel="stylesheet" type="text/css" media="all">

    <style>
      .watermark {
          position: absolute;
          color: grey;
          opacity: 0.30;
          font-size: 4em;
          width: 100%;
          text-align: center;
          z-index: -1;
          top: 20%;
          left: 50%;
          transform: translate(-50%, -50%) rotate(-45deg);
      }
  </style>
  
</head>

<body style="background:url('https://i.ibb.co/dQgj9sn/rollslip-bg.png');background-size: 320px 320px ;background-repeat: no-repeat;background-position: center center;">

  <div class="watermark">Mid Term Examinations Fall-2024 Semester</div>

  <div style="text-align: left; position: absolute; margin-left: -13px; margin-top: -13px; width: 80%;">
    <img src="data:image/png;base64,<?= $base64Image_tr ?>" alt="QR Code" style="width: 20%;">
  </div>

  @if(count($rollnoslip))
  <?php if(!empty($picture)){
				$blobimg = $picture;
		  ?>


              <div class="card-header">
                  <div class="pic" style="float:right !important">
		              <?php echo '<img src ="data:image/jpeg;base64,'.base64_encode($blobimg).'" id="pic1" src="#" alt="your image" class="img-thumb" width=80 height=80 border=1/>'; }?>
                 </div>
                  <h3 class="head1" style="text-align: center; margin-left:10%; padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom:0px;"><u>INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</u></h3>
                    <!-- <h4 class="head2" style="text-align: center; margin-left:10%; padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom:0px;"><u>DIRECTORATE OF ACADEMICS & EXAMINATIONS</u></h4> -->
                    <h4 class="head2" style="text-align: center; margin-left:10%; padding-top: 0px; padding-bottom: 0px; margin-top: 0px; margin-bottom:0px;"><u>(EXAMINATION BRANCH)</u></h4>
                    <h4 class="head3" style="text-align: center; margin-left:10%;padding-bottom: 0px;margin-bottom:0px;"><u>ROLL NO SLIP FOR MID TERM EXAMINATION</h4>  
                    {{-- <h4 class="head3" style="text-align: center; margin-left:10%;padding-bottom: 0px;margin-bottom:0px;"><u>ROLL NO SLIP FOR TERMINAL EXAMINATION</h4> --}}
                    <h4 class="head3" style="text-align: center; margin-left:5%; padding-top: 0px; margin-top: 0px;"><u>(<b>{{ $rollnoslip[0]->semcode }}</b>) SEMESTER</h4>
                    </u>
                  </h5>
               </div>
                <div class="card-body">
                   <div class="row">
                      <strong>
                        <u>Roll No. / Reg No#</u></b>&nbsp; <u>{{ $rollnoslip[0]->regno }}</u>
                      </strong>
                 </div>
                  <div class="card-body">
                     <p>Student Name Mr./Ms:&nbsp;<strong>{{ $rollnoslip[0]->studname }}</strong><hr class="hr1"/></p>
                     <p>Son/Daughter of: &nbsp;<strong>{{ $rollnoslip[0]->studfathername }}</strong><hr class="hr2"/></p>
                     <p>Faculty/Institute/School: &nbsp;<strong>{{ $faculty[0]->facname }}</strong><hr class="hr3"/></p>
                     <p>Degree Program: &nbsp;<strong>{{ $acadprog[0]->acadprogname }}</strong>&nbsp;<small>{{ $batch[0]->batchname }}</small><hr class="hr4"/></p>
                    <p>Examination to be held in: &nbsp;<strong>{{ 'Nov-2024' }}{{--date('M-Y', strtotime($semdate[0]->semenddate))--}}</strong><hr class="hr5"/></p>
                    <!-- <p>Issue Date: &nbsp;<strong><?php echo date("F-d-Y H:m:i A");?></strong><hr class="hr5"/></p> -->
                    <p>Issue Date: &nbsp;<strong><?php echo date("F-d-Y");?></strong><hr class="hr5"/></p>
                    <br>
                    <h4 class="head5"><u>DETAIL OF COURSES, ALLOWED TO BE APPEARED IN EXAM {{ $rollnoslip[0]->semcode }} {{-- SEMESTER --}}</u></h4>
                <table width="450">
                  <thead>
                    <tr>
                      <th style="width: 5px">S. No</th>
                      <th style="width: 5px">Course Code</th>
                      <th style="width: 10px">Course Title</th>
                      <th style="width: 10px">Remarks</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $sn=0;?>
                  @foreach($rollnoslip as $slips)
                  <?php $sn++?>
                    <tr>
                      <td style="width: 5px"><?php echo $sn?></td>
                      <td style="width: 5px;">{{ $slips->coursecode }}</td>
                      <td style="width: 10px">{{ $slips->coursename }}</td>
                      <td style="width: 10px text-align: center">{{ (strtolower($slips->crspreventstatus)=='p') ? 'Prevented' : '' }}</td>
                    </tr>
                   @endforeach
                  </tbody>
                </table>
          <div class="col-md-12">
          <b><u>Note:</u></b>
          <p>
          <ol type="i">
          <li>Bring this slip and your University Identity Card in the Examination Hall</li>
            <li>Possession of a Cell Phone/Digital Gagets in the Examination Hall is an offence as per University code of conduct.</li>
            <!-- <li>Your Registration Number will be considered as Roll No.</li> -->
            <li>All Roll No Slips are issued provisionally; therefore conduct of examination does not confer any right to a student to claim result of these courses.</li>
            <li>Students should not appear in the exams if having short attendance (less than 75%) in any course(s) and result shall be cancelled in case they appear in the examination.</li>
          @if($defaultstud)
          		<li>You are provisionally permitted to appear in the Mid Term Examinations Fall 2023; however be reminded/informed  that:-.</li><br><br>
               <b style="font-size:13px">&nbsp;&nbsp;&nbsp;&nbsp;i).  Your result for Semester Fall 2023 shall be withheld and you cannot claim for the results, based on this conditional  permission.
                </b><br><br>
                <b style="font-size:13px">&nbsp;&nbsp;&nbsp;&nbsp;ii).  Course registration for the next semester (Spring 24) shall not be allowed without clearance of fee default. Moreover, you shall not be allowed to attend classes without submission of outstanding fee.
                </b><br><br>
                <b style="font-size:13px">&nbsp;&nbsp;&nbsp; iii). You are also advised to clear/pay outstanding dues/fee at earliest to avoid any issues in the future.
                </b>
          @endif
		  </ul>
          </p>
          </div>

                  </div>
               </div>

          <i style="font-size:10px">Al-Jamia (IT-Center) Auto Generated on <?php echo date("M-d-Y H:m:i A");?></i>

    @else
        <strong class="text-danger">No Roll No Slip against your record, Please contact <a href="https://support.iiu.edu.pk/" target="_blank">LMS Support.</strong></a>
    @endif


</body>
</html>
