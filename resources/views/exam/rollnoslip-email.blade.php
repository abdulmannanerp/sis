@extends('adminlte::page')

@section('title', 'SIS | Dashboard')

@section('content_header')
    <h1>Welcome {{auth()->user()->name ?? '' }}</h1>
@stop

@section('content')
    <p> Bulk Email Roll No Slip to students.</p>
    <a href="{{ route('Sentemailrollnoslip') }}" class="btn btn-success float-left" style="margin-right: 5px;">
                    <i class="fas fa-envelope"></i> Email Roll No Slip to All
                  </a>
                  <br><br><br>
    @if(!empty($count))
    <blockquote class="quote-danger">
  <h5>Note!</h5>
  <p>Total No. of Roll No Slips Email to Students are : <strong>({{ $count }})</strong></p>
</blockquote>
    @endif 
@stop