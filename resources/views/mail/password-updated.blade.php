<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Password Updated</title>
</head>
<body>
    <strong>
        <p>Dear {{$user->name ?? ''}} </p>
    </strong>
    <p>
        Your password for <a href="https://sis.iiu.edu.pk">https://sis.iiu.edu.pk</a> has been updated. Your new password is
    </p>
    <p>
        Password: {{ $password }}
    </p>
    <p>
        Thank you.
    </p>
</body>
</html>