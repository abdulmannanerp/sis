@extends('adminlte::page')

@section('title', 'SIS | Dashboard')

@section('content_header')
    <h1>Welcome {{auth()->user()->name ?? '' }}</h1>
@stop

@section('content')
    <p>Please navigate to "Fee challans" menu to download your fee challan.</p>
    @if($errors->any())
    <h4 style="color:red">{{$errors->first()}}</h4>
    @endif
@stop
