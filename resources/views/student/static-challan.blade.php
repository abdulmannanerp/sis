<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Fee Challan</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{public_path('challan/download-fee-challan.css')}}" rel="stylesheet" type="text/css" media="all">  
</head>
<body>
  <div class="container" id="fee-challan-container">
    <div class="dues-challan">
        @php
            $generatedChallan = [];
        @endphp
        @foreach ( $challanDetails as $challan )
            @if ( ! in_array ( $challan->challanno, $generatedChallan ) )
                @foreach ( $copies as $copy ) 
                    @php $total = 0; @endphp
                    <div class="single-column">
                        <img src="{{public_path('images/challan-header.jpg')}}" class="header-image" alt="IIUI">
                        <div class="bank-logo-container">
                            @if ( $challan->gender == "F" && strpos(strtolower($challan->bankname), 'allied') !== false)
                                <img src="{{public_path('images/AlliedBank.png')}}" alt="Allied Bank Limited">
                            @elseif ($challan->gender == "M" && strpos(strtolower($challan->bankname), 'baraka') !== false)
                                <img src="{{public_path('images/albaraka.png')}}" alt="Albaraka">
                            @elseif ($challan->gender == "M" && strpos(strtolower($challan->bankname), 'habib') !== false)
                                <img src="{{public_path('images/Hbl.jpg')}}" alt="Habib Bank Limited">
                            @endif 
                        </div>
                        <div class="account-number">
                            <span class="bold "> {{ $challan->bankname ?? '' }} Account#: {{ $challan->bankaccno ?? '' }} </span>
                        </div>


                        {{-- Dev Mannan: Code of challan second bank starts --}}
                        <div style="padding-top:8px;padding-bottom:8px"><hr style="border:1px solid #ccc;"></div>
                        <div class="bank-logo-container">
                            @if ( $challan->gender == "F" && strpos(strtolower($feedetails->femalebank2), 'allied') !== false)
                                <img src="{{public_path('images/AlliedBank.png')}}" alt="Allied Bank Limited">
                                <div class="account-number">
                                    <span class="bold "> {{ $feedetails->femalebank2 ?? '' }} <br> Account# 1: {{ $feedetails->femaleaccountno2 ?? '' }} </span>
                                </div>
                            
                            @elseif ($challan->gender == "F" && strpos(strtolower($feedetails->femalebank2), 'alfalah') !== false)
                                <img src="{{public_path('images/alfalah2.jpg')}}" alt="alfalah">
                                <div class="account-number">
                                    <span class="bold "> {{ $feedetails->femalebank2 ?? '' }} <br> Payment through Alfalah Transact: <br> {{ $feedetails->femaleaccountno2 ?? '' }} </span>
                                </div>

                            @elseif ($challan->gender == "M" && strpos(strtolower($feedetails->malebank2), 'baraka') !== false)
                                <img src="{{public_path('images/albaraka.png')}}" alt="Albaraka">
                                <div class="account-number">
                                    <span class="bold "> {{ $feedetails->malebank2 ?? '' }} <br> Account# 2: {{ $feedetails->maleaccountno2 ?? '' }} </span>
                                </div>
                            @elseif ($challan->gender == "M" && strpos(strtolower($feedetails->malebank2), 'habib') !== false)
                                <img src="{{public_path('images/Hbl.jpg')}}" alt="Habib Bank Limited">
                                <div class="account-number">
                                    <span class="bold "> {{ $feedetails->malebank2 ?? '' }} <br> Account# 2: {{ $feedetails->maleaccountno2 ?? '' }} </span>
                                </div>
                            @elseif ($challan->gender == "M" && strpos(strtolower($feedetails->malebank2), 'faysal') !== false)
                            <img src="{{public_path('images/faysal.jpg')}}" alt="Faysal Bank Limited">
                            <div class="account-number">
                            <span class="bold "> {{ $feedetails->malebank2 ?? '' }} <br> Counter Payment Via Faysal Transact System: <br> {{ $feedetails->maleaccountno2 ?? '' }} </span>
                        </div>
                            @endif
                        </div>

                        {{-- Dev Mannan: Code of challan second bank Ends --}}
                        <table class="detail-table default-challan">
                            <tbody>
                                <tr>
                                    <td class="bold fixed-width">Challan#:</td>
                                    <td> {{ $challan->challanno }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">CNIC:</td>
                                    <td> {{ $challan->cnic ?? '' }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Semester:</td>
                                    <td> {{ $challan->semcode}} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Issued Date:</td>
                                    {{-- <td> {{ $challan->challandate }}</td> --}}
                                    <td> {{ date_format ( date_create ( $challan->challandate ) , 'd-m-Y' ) }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Due Date:</td>
                                    {{-- <td class="bold"> {{ $challan->challanduedate }} </td> --}}
                                    <td class="bold"> {{ date_format ( date_create ( $challan->challanduedate ) , 'd-m-Y' ) }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Reg#: </td>
                                    <td> {{ $challan->regno }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Name: </td>
                                    <td> {{ $challan->studentname ?? ''}} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Father's Name: </td>
                                    <td> {{ $challan->fathername ?? ''}} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Faculty: </td>
                                    <td> {{ $challan->facname }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Department: </td>
                                    <td> {{ $challan->depname }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Program: </td>
                                    <td> {{ $challan->programe }} </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="fee-distribution-table">
                            <thead>
                                <tr>
                                    <th class="fee-head-title">Particulars</th>
                                    <th class="border-left fee-head-amount">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $challanDetails as $singleChallan )
                                    @if ( $singleChallan->challanno == $challan->challanno ) 
                                        <tr>
                                            <td class="fee-head-title">{{$singleChallan->feedesc}}</td>
                                            <td class="border-left right-align fee-head-amount">{{ $singleChallan->feeamnt }}</td>
                                        </tr>
                                        @php $total += $singleChallan->feeamnt; @endphp
                                    @endif
                                @endforeach
                                <tr>
                                    <td class="bold total-amount">Total Amount (PKR)</td>
                                    <td class="border-left right-align">
                                        <span class="bold" id="dues_total_zer">
                                                {{ $total }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fee-head-title">Fine</td>
                                    <td class="border-left right-align fee-head-amount">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fee-head-title">Grand Total (PKR)</td>
                                    <td class="border-left right-align fee-head-amount">{{ $total }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="fine-instructions">
                            Late Payment Fine Per day Rs.100 upto Max. Rs. 5000 per Semester
                        </div>
                        <div class="bank-instructions">
                            <h4 class="bank-instructions-title">Instruction(s)</h5>
                            <ul class="bank-instructions-list">
                                <li>Can be deposited in any branch of Pakistan.</li>
                                 {{-- <li>All branches are requested to recieve the challan.</li>  --}}
                                {{-- <li>In case of any queries, Please contact fee section.</li> --}}
                            </ul>
                        </div>
                        <div class="copy-owner">
                            {{-- <div class="bank-title">{{ $accountNumber->bank_name ?? '' }} </div> --}}
                            <span class="copy">{{ $copy }}</span>
                        </div>
                    </div> <!-- Ending single-column --> 
                @endforeach
                <div class="page-break"></div>
            @endif
            @php 
                $generatedChallan[] = $challan->challanno;
            @endphp
        @endforeach 
    </div> <!-- Ending dues challan --> 
  </div>
</body>
</html>