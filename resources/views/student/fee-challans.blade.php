@extends('adminlte::page')

@section('title', 'SIS | Fee Challans')

@section('content_header')
    <h1>Fee Challans</h1>
@stop

@section('content')
    @if($challan)
        <h5>Semester Fee Challan</h5>
        <a href="{{ route('downloadFeeChallan') }}" target="_blank">Download Semester Fee Challan</a>
    @else
        <strong class="text-danger">No challan found against your record, Please contact fee section.</strong>
    @endif
@stop
