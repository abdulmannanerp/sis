@extends('adminlte::page')

@section('title', 'Fee | Misc Challan')

@section('content_header')
    <h1>Miscellaneous Challan</h1>
@stop

@section('content')
    
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Fee Challan Category</h3>
    </div>
    <form role="form" action="{{ route('viewMisChallan')}}" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label>Select Challan</label>
          <select class="form-control" name="feecode" required>
            <option value="">Select Challan Category</option>
            @foreach ($feeCodes as $code)
              <option value="{{ $code->feecode }}">{{ $code->feedesc }}</option>
            @endforeach
          </select>
        </div>
        <div class="from-group">
          <button type="submit" class="btn btn-primary" >View Challan</button>
        </div>
      </div>
    </form>
  </div>
@endsection