<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Fee Challan</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{public_path('challan/download-fee-challan.css')}}" rel="stylesheet" type="text/css" media="all">
</head>
<body>
  <div class="container" id="fee-challan-container">
    <div class="dues-challan">
        @php
            $generatedChallan = [];
        @endphp
        @foreach ( $challanDetails as $challan )
            @if ( ! in_array ( $challan->challanno, $generatedChallan ) )
                @foreach ( $copies as $copy )
                    @php $total = 0; @endphp
                    <div class="single-column">
                        <img src="{{public_path('images/challan-header.jpg')}}" class="header-image" alt="IIUI">





                        <div class="bank-logo-container">
                            @if ( $challan->gender == "F" && strpos(strtolower($challan->bk2name), 'allied') !== false)
                                <img src="{{public_path('images/AlliedBank.png')}}" alt="Allied Bank Limited">
                                <div class="account-number">
                                    <span class="bold "> {{ $challan->bankname ?? '' }} <br> Account# 1: {{ $challan->bankaccno ?? '' }} </span>
                                </div>
                            
                            @elseif ($challan->gender == "F" && strpos(strtolower($challan->bk2name), 'alfalah') !== false)
                                <img src="{{public_path('images/alfalah2.jpg')}}" alt="alfalah">
                                <div class="account-number">
                                    <span class="bold "> {{ $challan->bk2name ?? '' }} <br> Payment through Alfalah Transact: <br> {{ $challan->bkacc ?? '' }} </span>
                                </div>
                            @elseif ($challan->gender == "M" && strpos(strtolower($challan->bk2name), 'baraka') !== false)
                                <img src="{{public_path('images/albaraka.png')}}" alt="Albaraka">
                                <div class="account-number">
                                    <span class="bold "> {{ $challan->bk2name ?? '' }} <br> Account# 2: {{ $challan->bkacc ?? '' }} </span>
                                </div>
                            @elseif ($challan->gender == "M" && strpos(strtolower($challan->bk2name), 'habib') !== false)
                                <img src="{{public_path('images/Hbl.jpg')}}" alt="Habib Bank Limited">
                                <div class="account-number">
                                    <span class="bold "> {{ $challan->bk2name ?? '' }} <br> Account# 2: {{ $challan->bkacc ?? '' }} </span>
                                </div>
                            @elseif ($challan->gender == "M" && strpos(strtolower($challan->bk2name), 'faysal') !== false && false)
                            <img src="{{public_path('images/faysal.jpg')}}" alt="Faysal Bank Limited">
                            <div class="account-number">
                            <span class="bold "> {{ $challan->bk2name ?? '' }} <br> Counter Payment Via Faysal Transact System: <br> {{ $challan->bkacc ?? '' }} </span>
                        </div>
                            @endif
                        <!-- <div class="account-number">
                            <span class="bold "> {{ $challan->bankname ?? '' }} <br> Account# 1: {{ $challan->bankaccno ?? '' }} </span>
                        </div> -->
                        <div style="padding-top:8px;padding-bottom:8px"><hr style="border:1px solid #ccc;"></div>


                            <div class="bank-logo-container">
                                @if ( $challan->gender == "F" && strpos(strtolower($challan->bankname), 'allied') !== false)
                                    <img src="{{public_path('images/AlliedBank.png')}}" alt="Allied Bank Limited">
                                    <div class="account-number">
                                        <span class="bold "> {{ $challan->bankname ?? '' }} <br> Account# 1: {{ $challan->bankaccno ?? '' }} </span>
                                    </div>
                                @elseif ($challan->gender == "M" && strpos(strtolower($challan->bankname), 'baraka') !== false)
                                    <img src="{{public_path('images/albaraka.png')}}" alt="Albaraka">
                                    <div class="account-number">
                                        <span class="bold "> {{ $challan->bankname ?? '' }} <br> Account# 1: {{ $challan->bankaccno ?? '' }} </span>
                                    </div>
                                @elseif ($challan->gender == "M" && strpos(strtolower($challan->bankname), 'habib') !== false)
                                    <img src="{{public_path('images/Hbl.jpg')}}" alt="Habib Bank Limited">
                                    <div class="account-number">
                                        <span class="bold "> {{ $challan->bankname ?? '' }} <br> Account# 1: {{ $challan->bankaccno ?? '' }} </span>
                                    </div>
                                @endif
                            </div>





                        </div>
                        <!-- <div class="account-number">
                            <span class="bold "> {{ $challan->bk2name ?? '' }} <br> Account# 2: {{ $challan->bkacc ?? '' }} </span>
                        </div> -->
                        <table class="detail-table default-challan" style="margin-top:10px">
                            <tbody>
                                <!-- <tr>
                                    <td class="bold fixed-width">Bank# 2:</td>
                                    <td> {{ $challan->bk2name }} </td>
                                </tr>

                                <tr>
                                    <td class="bold fixed-width">Account# 2:</td>
                                    <td> {{ $challan->bkacc }} </td>
                                </tr> -->
                                
                                <tr>
                                    <td class="bold fixed-width">Challan#:</td>
                                    <td> {{ $challan->challanno }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">CNIC:</td>
                                    <td> {{ $challan->cnic ?? '' }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Semester:</td>
                                    <td> {{ $challan->semcode}} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Issued Date:</td>
                                    <td>
                                        {{ date_format ( date_create ( $challan->challandate ) , 'd-m-Y' ) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Due Date:</td>
                                    {{-- <td class="bold"> 16-07-2021 </td> --}}
                                    <td class="bold"> {{ date_format ( date_create ( $challan->challanduedate ) , 'd-m-Y' ) }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Reg#: </td>
                                    <td> {{ $challan->regno }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Name: </td>
                                    <td> {{ $challan->studentname ?? ''}} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Father's Name: </td>
                                    <td> {{ $challan->fathername ?? ''}} </td>
                                </tr>
                                {{-- <tr>
                                    <td class="bold">Faculty: </td>
                                    <td> {{ $challan->facname }} </td>
                                </tr> --}}
                                <tr>
                                    <td class="bold">Department: </td>
                                    <td> {{ $challan->depname }} </td>
                                </tr>
                                <tr>
                                    <td class="bold">Program: </td>
                                    <td> {{ $challan->programe }} </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="fee-distribution-table">
                            <thead>
                                <tr>
                                    <th class="fee-head-title">Particulars</th>
                                    <th class="border-left fee-head-amount">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php 
                                    $showHideFine = 0;
                                @endphp
                                @foreach ( $challanDetails as $singleChallan )
                                    
                                    @if ( $singleChallan->challanno == $challan->challanno )
                                        @php
                                            if($singleChallan->feedesc == 'University Dues/Tuition Fee'){
                                                $showHideFine = 1;
                                            }
                                        @endphp
                                        <tr>
                                            <td class="fee-head-title">{{$singleChallan->feedesc}}</td>
                                            <td class="border-left right-align fee-head-amount">{{ $singleChallan->feeamnt }}</td>
                                        </tr>
                                        @php $total += $singleChallan->feeamnt; @endphp
                                    @endif
                                @endforeach
                                <tr>
                                    <td class="bold total-amount">Total Amount (PKR)</td>
                                    <td class="border-left right-align">
                                        <span class="bold" id="dues_total_zer">
                                                {{ $total }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fee-head-title">Fine</td>
                                    <td class="border-left right-align fee-head-amount">
                                        @php
                                            // $fine = 5000;
                                            $fine = 0;
                                            if (isset($fineAmount)) {
                                                $fine = $fineAmount;
                                            }
                                            if (! isset($fineAmount)) {
                                                $challanLastDate = \Carbon\Carbon::parse($challan->challanduedate);
                                                $challanLastDateToTime = strtotime($challanLastDate);
                                                $dateNow = strtotime(\Carbon\Carbon::now()->toDateString());
                                                if ($dateNow > $challanLastDateToTime) {
                                                    $difference = $challanLastDate->diffInDays(\Carbon\Carbon::now());
                                                    $fine = $difference * 100;
                                                    if ( $fine > 5000 ) {
                                                        $fine = 5000;
                                                    }
                                                    // if ( $fine > 200 ) {
                                                    //     $fine =  $fine - 200;
                                                    // }
                                                }
                                            }
                                            if($showHideFine == 1){
                                                echo $fine;
                                            }
                                            
                                        @endphp
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fee-head-title">Grand Total (PKR)</td>
                                    <td class="border-left right-align fee-head-amount">
                                        @php
                                            if($showHideFine == 1){
                                                echo $fine + $total;
                                            }
                                            if($showHideFine == 0){
                                                echo $total;
                                            }
                                        @endphp
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="fine-instructions">
                            Late Payment Fine Per day Rs.100 upto Max. Rs. 5000 per Semester
                        </div>
                        <div class="bank-instructions">
                            <h4 class="bank-instructions-title">Note: </h5>
                            <ul class="bank-instructions-list">
                                <li>Can be deposited in any branch of Pakistan.</li>
                            {{-- <li>Please enter full fee challan# at the time of punching.</li> --}}
                                {{-- <li>All branches are requested to recieve the challan.</li>
                                <li>In case of any queries, Please contact fee section.</li> --}}
                            </ul>
                        </div>
                        <div class="copy-owner">
                            {{-- <div class="bank-title">{{ $accountNumber->bank_name ?? '' }} </div> --}}
                            <span class="copy">{{ $copy }}</span>
                        </div>
                    </div> <!-- Ending single-column -->
                @endforeach
                <div class="page-break"></div>
            @endif
            @php
                $generatedChallan[] = $challan->challanno;
            @endphp
        @endforeach
    </div> <!-- Ending dues challan -->
    {{-- @if ( !($program->faculty->pkFacId  == 2 && $gender == 3) )
    @if($challan_security->total != 0)
    <div class="security-challan {{($challan_dues->total == 0)? 'security-challan-update' : '' }}">
        @foreach ( $copies  as $copy )
            <div class="single-column">
                <img src="{{public_path('images/challan-header.jpg')}}" class="header-image" alt="IIUI">
                <div class="bank-logo-container">
                    @if ( $gender == 2)
                        <img src="{{public_path('images/AlliedBank.png')}}" alt="Allied Bank Limited">
                    @elseif ($gender == 3 && $program->faculty->pkFacId  == 2)
                        <img src="{{public_path('images/albaraka.png')}}" alt="Albaraka">
                    @else
                        <img src="{{public_path('images/Hbl.jpg')}}" alt="Habib Bank Limited">
                    @endif
                </div>
                <div class="account-number">
                    <span class="bold "> {{ $accountNumber->bank_name ?? '' }} Account#: {{ $accountNumber->security_account ?? '' }} </span>
                </div>
                <table class="detail-table">
                    <tbody>
                        <tr>
                            <td class="bold fixed-width">Challan#:</td>
                            <td>
                            @if($challan_security->second_challan)
                                {{$challan_security->second_challan}}
                            @else
                                {{$challan_security->id ?? $challan_security->second_challan}}
                            @endif

                            </td>
                        </tr>
                        <tr>
                            <td class="bold">Semester:</td>
                            <td>{{ $semester->title ?? ''}}</td>
                        </tr>
                        <tr>
                            <td class="bold">Issued Date:</td>
                            <td>{{ $issuedDate ?? \Carbon\Carbon::now()->format('d-M-Y') }}</td>
                        </tr>
                        <tr>
                            <td class="bold">Due Date:</td>
                            <td class="bold">{{ date_format(date_create($feeSubmissionLastDate), 'd-M-Y') }}</td>
                        </tr>
                        <tr>
                            <td class="bold">Roll No: </td>
                            <td>{{ $rollno }}</td>
                        </tr>
                        <tr>
                            <td class="bold">Name: </td>
                            <td>{{ $applicant->applicant->name ?? ''}}</td>
                        </tr>
                        <tr>
                            <td class="bold">Father's Name: </td>
                            <td>{{ $applicant->applicant->detail->fatherName ?? ''}}</td>
                        </tr>
                        <tr>
                            <td class="bold">Faculty:</td>
                            <td>{{ $applicant->application->program->faculty->title }}</td>
                        </tr>
                        <tr>
                            <td class="bold">Department:</td>
                            @if (
                                  $applicant->application->fkProgramId == '189' ||
                                  $applicant->application->fkProgramId == '55' ||
                                  $applicant->application->fkProgramId == '56' ||
                                  $applicant->application->fkProgramId == '133'
                                )
                                <td>{{ $result->result->program->department->title }} </td>
                            @else
                                <td>{{ $applicant->application->program->department->title }}</td>
                            @endif
                        </tr>
                        <tr>
                            <td class="bold">Program: </td>
                             @if (
                                  $applicant->application->fkProgramId == '189' ||
                                  $applicant->application->fkProgramId == '55' ||
                                  $applicant->application->fkProgramId == '56' ||
                                  $applicant->application->fkProgramId == '133'
                                )
                                <td>{{ $result->result->program->title }} </td>
                            @else
                                <td>{{ $applicant->application->program->title }} </td>
                            @endif
                        </tr>
                    </tbody>
                </table>

                <table class="fee-distribution-table">
                    <thead>
                        <tr>
                            <th>Particulars</th>
                            <th class="border-left">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Library Security</td>
                            <td class="border-left right-align">{{ $challan_security->library_security ?? $fee->library_security }}</td>
                        </tr>
                        <tr>
                            <td>Book Bank Security</td>
                            <td class="border-left right-align">{{ $challan_security->book_bank_security ?? $fee->book_bank_security }}</td>
                        </tr>
                        <tr>
                            <td>Caution Money</td>
                            <td class="border-left right-align">{{ $challan_security->caution_money ?? $fee->caution_money }}</td>
                        </tr>
                        <tr>
                            <td>Hostel Security</td>
                            <td class="border-left"></td>
                        </tr>
                        <tr>
                            <td class="bold total-amount">Total Amount (PKR)</td>
                            <td class="border-left right-align bold" id="security_total_zer">
                                @if($challan_security)
                                {{ $challan_security->library_security + $challan_security->book_bank_security + $challan_security->caution_money  }}
                                @else
                                {{ $fee->library_security + $fee->book_bank_security + $fee->caution_money  }}
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="bank-instructions">
                    <h4 class="bank-instructions-title">Instructions</h5>
                    <ol class="bank-instructions-list">
                        <li>Please enter full fee challan# at the time of punching.</li>
                        <li>All branches are requested to receive the challan.</li>
                        <li>In case of any queries, Please contact fee section.</li>
                    </ol>
                </div>
                <div class="copy-owner">
                    <span class="copy">{{ $copy }}</span>
                </div>
            </div>
        @endforeach
    </div>
    @endif
    @endif --}}
  </div>
</body>
</html>