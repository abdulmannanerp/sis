<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Fee Challan</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{public_path('challan/download-fee-challan.css')}}" rel="stylesheet" type="text/css" media="all">
</head>
<body>
  <div class="container-fluid" id="fee-challan-container">
    <div style="text-align: right;">
        <img src="data:image/png;base64,<?= $qr_degree ?>" alt="QR Code" style="width: 15%;">
    </div>
    <div style="text-align: right; margin-top: 900px;">
        <img src="data:image/png;base64,<?= $qr_transcript ?>" alt="QR Code" style="width: 15%;">
    </div>
  </div>
</body>
</html>
