<!DOCTYPE html>
<html lang="en">
<head>
  <title>Student Info</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
  <style>
        .div-table {
            display: flex;
            flex-direction: column;
            border: 1px solid #e0e0e0;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
            overflow: hidden;
            border-radius: 5px;
        }

        .txt-xcc{
          text-align: center;
        }
        .div-table-row {
            display: flex;
            align-items: center;
        }

        /* .div-table-header {
            background-color: #007BFF;
            color: white;
        } */

        .div-table-cell {
            flex: 1;
            padding: 10px;
            border-bottom: 1px solid #e0e0e0;
            text-align: center;
        }

        .div-table-row:last-child .div-table-cell {
            border-bottom: none;
        }

        @media (max-width: 768px) {
            .div-table-cell {
                text-align: left;
                display: block;
            }

            /* .div-table-row {
                flex-direction: column;
            } */
        }
    </style>
</head>


<body>
<div class="border border-success p-3">

 <div class="txt-xcc"> 
  <img src="https://upload.wikimedia.org/wikipedia/en/c/ca/IIU_Logo.png" alt="IIU Logo" width="100">
 </div>

<h2 class="headingOne text-center"><b>INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</b></h2>
  <!-- Top Table Start -->
  <?php /* ?>
  <div class="headingOne text-center">
    <?php echo '<img src ="data:image/jpeg;base64,'.base64_encode($blobimg).'" id="pic1" src="#" alt="your image" class="img-thumb" width=105 height=105 border=1/>'; ?>
  </div>
  <?php */ ?>
    <div class="div-table">

      <div class="div-table-row">
          
        <div class="div-table-cell"><b>Student Name</b></div>
        <div class="div-table-cell"><?= $semcode[0]->studname ?></div>

      </div>

      <div class="div-table-row">

        <div class="div-table-cell"><b>Father Name</b></div>
        <div class="div-table-cell"> <?= $semcode[0]->studfathername ?></div>

      </div>

       
        <div class="div-table-row">

            <div class="div-table-cell"><b>Faculty</b></div>
            <div class="div-table-cell"><?= $semcode[0]->facname ?></div>

        </div>

        <div class="div-table-row">

            <div class="div-table-cell"><b>Department</b></div>
            <div class="div-table-cell"> <?= $semcode[0]->depname ?></div>

        </div>

        <div class="div-table-row">

          <div class="div-table-cell"><b>Programme</b></div>
          <div class="div-table-cell"> <?= $semcode[0]->acadprogname ?></div>

        </div>

        <div class="div-table-row">

          <div class="div-table-cell"><b>Registration Number</b></div>
          <div class="div-table-cell"><?= $semcode[0]->regno ?></div>

        </div>


        <div class="div-table-row">

          <div class="div-table-cell"><b>Batch</b></div>
          <div class="div-table-cell"> <?= $semcode[0]->batchname ?></div>

        </div>

        <?php
        $status = '';
        if($semcode[0]->progcmpltdate  != ''){

          $status = 'Degree is Completed';

        }else{

          if($semcode[0]->status == 'CWC'){

            $status = 'Course Work Complete';
  
          }elseif($semcode[0]->status == 'P2C'){
  
            $status = 'Probation 2 Continued';
  
          }elseif($semcode[0]->status == 'P1'){
  
            $status = 'Probation 1 Continued';
  
          }elseif($semcode[0]->status == 'AC'){
  
            $status = 'Already Ceased';
  
          }elseif($semcode[0]->status == 'P2'){
  
            $status = 'Probation 2 Continued';
  
          }elseif($semcode[0]->status == 'RL'){
  
            $status = 'Result Later on';
  
          }elseif($semcode[0]->status == 'DC'){
  
            $status = 'Degree is Completed';
  
          }elseif($semcode[0]->status == 'C'){
  
            $status = 'Ceased';
  
          }elseif($semcode[0]->status == 'P3C'){
  
            $status = 'Probation 3 Ceased';
  
          }elseif($semcode[0]->status == 'G'){
  
            $status = 'Degree in Progress';
  
          }elseif($semcode[0]->status == 'AbC'){
  
            $status = 'Absent Ceased';
  
          }else{
            $status = '';
          }

        }


        ?>

        <div class="div-table-row">

          <div class="div-table-cell"><b>Status</b></div>
          <div class="div-table-cell"> <?= $status ?></div>

        </div>

        <div class="div-table-row">

          <div class="div-table-cell"><b>CGPA</b></div>
          <div class="div-table-cell"> <?= bcdiv($semcode[0]->cgpa, 1, 2) ?></div>

        </div>


        <?php
        /*
        if(count($rollnoslip)){
          ?>
          <h4 class="head5"><u>DETAIL OF COURSES, ALLOWED TO BE APPEARED IN EXAM <?= $rollnoslip[0]->semcode ?> </u></h4>
          <table width="450">
            <thead>
              <tr>
                <th style="width: 5px">S. No</th>
                <th style="width: 5px">Course Code</th>
                <th style="width: 10px">Course Title</th>
                <th style="width: 10px">Remarks</th>
              </tr>
            </thead>
            <tbody>
            <?php $sn=0;?>
            <?php 
            foreach($rollnoslip as $slips){
            ?>
            <?php $sn++?>
              <tr>
                <td style="width: 5px"><?= $sn ?></td>
                <td style="width: 5px;"><?= $slips->coursecode ?> </td>
                <td style="width: 10px"><?= $slips->coursename ?> </td>
                <td style="width: 10px text-align: center"><?= (strtolower($slips->crspreventstatus)=='p') ? 'Prevented' : '' ?></td>
              </tr>
            <?php
            }
            ?>
            </tbody>
          </table>
          <?php
          }
          */
          ?>


    </div>

</div>
</body>
</html>















