@extends('adminlte::page')
@section('title', 'Fee | View Misc Challan')
@section('content_header')
  <h1>View Miscellaneous Challan</h1>
@stop
@section('content')
  <div class="alert alert-danger alert-dismissible">
    <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
    Note: <strong>Once Challan has been generated, Will not be cancelled and should be paid by student.</strong>
  </div>
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">View Challan Info</h3>
  </div>
  <form role="form" action="{{ route('generateChallan')}}" method="POST">
    @csrf
    {{-- @foreach ( $challanDetails as $challan ) --}}
    <div class="card-body">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label>Reg No.</label>
            <input type="text" class="form-control" value="{{ auth()->user()->regno }}" disabled="">
            <input type="hidden" class="form-control" value="{{ $feedetails->feecode }}" name="feecode">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label>Due Date.</label>
            <input type="text" class="form-control" value="{{ Date('d-m-Y', strtotime('+'.$feedetails->nofdays.' days')) }}" disabled="">
          </div>
        </div>
        {{-- <div class="col-sm-6">
          <div class="form-group">
            <label>Father Name.</label>
            <input type="text" class="form-control" value="{{ $challan->fathername ?? ''}}" disabled="">
          </div>
        </div> --}}
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label>Challan Type.</label>
            <input type="text" class="form-control" value="{{ $feedetails->feedesc }}" disabled="">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label>Challan Amount.</label>
            <input type="text" class="form-control" value="{{ $feedetails->feeamount ?? ''}}" disabled="">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label>Bank Name.</label>
            @if ( $feedetails->gender == "F")
            <input type="text" class="form-control" value="{{ $feedetails->femalebank ?? '' }}" disabled="">
            @elseif ( $feedetails->gender == "M")
            <input type="text" class="form-control" value="{{ $feedetails->malebank ?? '' }}" disabled="">
            @endif
          </div>
        </div>
      </div>
      <button type="submit" class="btn btn-primary" onclick="return confirmation();" >Generate Fee Challan</button>
    </div>
    {{-- @endforeach --}}
  </form>
  
  
  <script type="text/javascript">
    function confirmation() {
      if(confirm('Are you sure you want to generate challan?')) {
        return true;
      }
      return false;
    }
  </script>
@endsection