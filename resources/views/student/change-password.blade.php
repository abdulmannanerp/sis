@extends('adminlte::page')

@section('title', 'SIS | Change Password')

@section('content_header')
    <h1>Change Password</h1>
@stop

@section('content')
    <div class="col-sm-12 col-md-6">
        @if( $status )
            <div class="alert alert-success">
                {{ $status }}
            </div>
        @endif
        @if ( $errors->any() )
            @foreach( $errors->all() as $error ) 
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        @endif
        <div class="box box-info">
            <form action="{{ route('updatePassword')}}" method="POST">
                @csrf
                <input type="hidden" name="ssoUserId" value="{{$user->sso_user_id}}">
                <input type="hidden" name="ssoAuthToken" value="{{$user->sso_auth_token}}">
                <div class="form-group">
                    <label for="#">New Password</label>
                    <input type="password" class="form-control" name="password" required>
                </div>
                <div class="form-group">
                    <label for="#">Confirm Password</label>
                    <input type="password" class="form-control" name="password_confirmation" required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-sm" value="Change Password">
                </div>
            </form>
        </div>
    </div>
@stop