@extends('voyager::master')

@section('page_title', 'Fee Verification')

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-dollar"></i> Fee Verification
    </h1>
</div>
@stop

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <form action="{{ route('feeVerify') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="#">Enter Challan Numbers (Seperated by Comma or New Line)</label>
                            <textarea class="form-control" rows="7" name="challans"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="List Records">
                        </div>
                    </form>
                    @if ($challans)
                        <table class="table table-striped">
                            <thead class="thead thead-dark">
                                <tr>
                                    <td>Challan#</td>
                                    <td>Reg No</td>
                                    <td> Name </td>
                                    <td>Semester</td>
                                    <td>Head</td>
                                    <td>Amount</td>
                                    <td>Verified At</td>
                                    <td>Verified By</td>
                                </tr>
                            </thead>
                            <tbody>
                               
                                @foreach ( $challans as $challan )
                                    <tr>
                                        <td> {{ $challan->challanno }} </td>
                                        <td> {{ $challan->regno }} </td>
                                        <td> {{ $challan->user->studname ?? '' }} </td>
                                        <td> {{ $challan->semcode }} </td>
                                        <td> {{ $challan->detail[0]->fee_code->feedesc }} </td>
                                        <td> {{ $challan->challanamnt }} </td>
                                        <td> {{ $challan->challanpaiddatetime }}</td>
                                        <td> {{ $challan->challanverifiedby }} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <form action="{{ route('verifyChallans') }}" method="POST">
                            @csrf
                            @php
                                $challanNumbers = [];
                                foreach ( $challans as $challan ) {
                                    $challanNumbers[] = $challan->challanno;
                                }
                            @endphp
                            <input type="hidden" name="challanNumbers" value="{{ implode(',', $challanNumbers) }}">
                            <input type="submit" class="btn btn-primary" value="Verify">
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop