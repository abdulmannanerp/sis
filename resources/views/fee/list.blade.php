@extends('voyager::master')

@section('page_title', 'Block Fee Generation')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-dollar"></i> Students List
        </h1>
    </div>
@endsection

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Reg Number</th>
                                    <th>Issue Date</th>
                                    <th>Due Date</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($students as $student)
                                    <tr>
                                        <td>{{ $student->studname}}</td>
                                        <td>{{ $student->regno}}</td>
                                        <td>{{ \Carbon\Carbon::now()->format('d-M-Y h:i:s a') }}</td>
                                        <td>{{ date_format(date_create($request->date), 'd-M-Y') }}</td>
                                        <td>{{ $request->amount }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <a href="{{ route('saveChallan') }}" class="btn btn-primary">Generate Challan</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection