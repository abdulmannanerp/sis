@extends('voyager::master')

@section('page_title', 'Block Fee Generation')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-dollar"></i> Block Fee Generation
        </h1>
    </div>
@endsection

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <form action="{{ route('blockFee') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Academic Program Name</label>
                            <select class="form-control select2" id="program">
                                <option value="">--Select Academic Program--</option>
                                @foreach ($programs as $program)
                                    <option value="{{$program->acadprogcode}}"> {{ $program->acadprogname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- <option value="SECODEMbse5354">BS Economics F20</option> -->
                        <div class="form-group">
                            <label>Batch Name</label>
                            <select class="form-control select2" name="batch" id="batch">
                                <option value="">--Select Batch--</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Fee/Challan Type</label>
                            <select class="form-control">
                                <option value="">--Select Fee Type--</option>
                                <option value="semester-fee">Semester Fee</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Semester</label>
                            <select class="form-control select2">
                                <option value="">--Select Semester--</option>
                                @foreach ($semesters as $semester)
                                    <option value="{{$semester->semcode}}"> {{ $semester->semcode }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Block Fee Amount</label>
                            <input type="number" name="amount" class="form-control" placeholder="50000">
                        </div>
                        <div class="form-group">
                            <label>Challan Due Date</label>
                            <input type="date" name="date" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-primary" value="Generate Challan">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(function(){
            $('#program').on('change', function(){
                let data = {
                    'program' : $(this).val()
                };
                fetch('/batch', {
                    method : 'POST',
                    credentials: 'same-origin',
                    headers : {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content'),
                    },
                    body : JSON.stringify(data)
                })
                .then(response => response.json())
                .then(response => {
                    let options = `<option value="">--Select Batch--</option>`;
                    response.forEach(function(item, index) {
                        options += `<option value="${item.batchcode}"> ${item.batchname}</option>`
                    });
                    $('#batch').empty().append(options);
                }).catch(error => {
                    console.log (error);
                });
            });
        });
    </script>
@endsection