<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Semester Result</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{public_path('permission-form/download-permission-form.css')}}" rel="stylesheet" type="text/css" media="all">
<style>
  @media print {
	  @page { size: [portrait]; }
}
@page { margin: 0px; }
body { margin: 0px; }
.container {
	/* width: 100%; */
	margin: 0 auto;
	font-family: 'Open Sans', sans-serif;
}
#footer { position: fixed; left: 0px; bottom: 20px; right: 0px; }
.date-div{
  text-align: center;
  float:right;
}
.headingOne{
  clear: both;
  text-align: center;
  font-size:15px;
}
.headingTwo{
  text-align: center;
  font-size:14px;
  text-decoration: underline;
}
.no-border-table{
  width:100%;
}
.no-border-table td{
  border: 0px;
  font-size:16px;
}
.no-border-table th{
  border: 0px;
  font-size:16px;
}
.no-border-table strong{
  border: 0px;
  font-size:16px;
}
.smooth-border{
  border:2px solid #000000;
  margin-top:5px;
}
.perm-tab th{
  border: 1px solid #333;
  text-align: left;
  padding: 2px;
}
.perm-tab td{
  border: 1px solid #333;
  text-align: left;
  padding: 2px;
}
table td{
  border: 0px;
  padding: 2px;
}
table {
  border-collapse: collapse;
  align:"center"
}
th, td, strong {
  font-size: 10px;
}
div, strong {
  font-size: 10px;
}
</style>
</head>
  <body>
      @php
  $date = Carbon\Carbon::now();
  $date = date_format($date,'d M Y');
  $i = 0;
  if(empty($regno)){
    @endphp
    <h1 style="text-align: center;" target_blank><b>No Registration Number Found, Kindly Visit Automation Center</b></h1>
    @php
  }else{
  $currentSemester = '';
  foreach($semcode as $key=>$year)
    if ($i == 0) {
      $currentSemester = $year->semcode;
      break;
    }
    $coursecodes=array();
@endphp
<div style="background:url('{{public_path('images/result-intimation-background.jpg')}}');height:250px;width:100%;background-size:cover;"><div style="padding-top:225px;padding-left:150px;font-size:25px;font-weight:bold;text-align:center">SPR-2022</div></div>
  <table style="width:100%;margin-left:20px;margin-right:20px;margin-top:20px">
      <tr>
          <td style="width:75%">
              <table style="width:100%;">
                  <tr>
                      <td style="font-weight:bold;font-size:20px;width:150px;">Registration No:</td>
                      <td style="border-bottom:1px solid #000;font-size:20px;">{{ $year->regno }}</td>
                  </tr>
                  <tr>
                      <td style="font-weight:bold;font-size:20px;width:140px;">Student Name:</td>
                      <td style="border-bottom:1px solid #000;font-size:20px;">{{ $year->studname }}</td>
                  </tr>
                  <tr>
                      <td style="font-weight:bold;font-size:20px;width:140px;">Father's Name:</td>
                      <td style="border-bottom:1px solid #000;font-size:20px;">{{ $year->studfathername }}</td>
                  </tr>
                  <tr>
                      <td style="font-weight:bold;font-size:20px;width:100px;">Faculty:</td>
                      <td style="border-bottom:1px solid #000;font-size:20px;">{{ $year->facname }}</td>
                  </tr>
                  <tr>
                      <td style="font-weight:bold;font-size:20px;width:100px;">Department:</td>
                      <td style="border-bottom:1px solid #000;font-size:20px;">{{ $year->depname }}</td>
                  </tr>
                  <tr>
                      <td style="font-weight:bold;font-size:20px;width:100px;">Degree Program:</td>
                      <td style="border-bottom:1px solid #000;font-size:20px;">{{ $year->acadprogname }}</td>
                  </tr>
              </table>
          </td>
          <td style="text-align:center">@php echo '<img src ="data:image/jpeg;base64,'.base64_encode($picture).'" id="pic1" src="#" alt="your image" class="img-thumb" width=105 height=105 border=1/>'; @endphp
                 </div> </td>
      </tr>
  </table>
  @php
            $currentSemester='';
          @endphp
            @foreach($semcode as $key=>$year)
            @php
            if(in_array($year->coursecode,$coursecodes)){
            continue;
            }
            $coursecodes[]=$year->coursecode;
                if($year->semcode!='FALL-2023'){
                    continue;
                }
            @endphp
               @if(empty($currentSemester))
                @php
                $currentSemester=$year->semcode;
              @endphp
              <table class="table table-hover" style="width:100%;margin-left:20px;margin-right:20px;margin-top:30px">
                <thead class="thead-dark">
                  <tr>
                      <th style="width: 12%;font-weigh:bold;font-size:15px">Course Code</th>
                      <th style="font-weigh:bold;font-size:15px;text-align:left">Course Title</th>
                      <th style="width: 7%;font-weigh:bold;font-size:15px">Cr. Hrs</th>
                      <th style="width: 7%;font-weigh:bold;font-size:15px">Grade</th>
                  </tr>
                </thead>
                <tbody>
                     @elseif($currentSemester!=$year->semcode)
                      @php
                          $currentSemester=$year->semcode;
                       @endphp
                </tbody>
              </table>
              <div><strong>Semester </strong> {{ $year->semcode }}</div>
                 <table class="table table-hover" style="width:100%">
                    <thead class="thead-dark">
                      <tr>
                          <th style="width: 12%;">Code</th>
                          <th style="width: 35%;">Course Title</th>
                          <th style="width: 12%;">Cr. Hrs</th>
                          <th style="width: 12%;">Grade</th>

                      </tr>
                  </thead>
                  <tbody>
                         @endif
                        <tr>
                          <td style="border:1px solid #000;font-size:15px">{{ $year->coursecode }}</td>
                          <td style="border:1px solid #000;font-size:15px">{{ $year->coursename }}</td>
                          <td style="border:1px solid #000;text-align:center;font-size:15px">{{ $year->credithrs }}</td>
                          <td style="border:1px solid #000;text-align:center;font-size:15px">
                            @if($year->semcode!='FALL-2021' )
                          {{ $year->grade }}
                        @endif
                        </td>
                        </tr>
                      @endforeach

                  </tbody>
              </table>
              <table style="width:100%;margin-left:20px;margin-right:20px;margin-top:20px">
                  <tr>
                      <td style="font-weight:bold;font-size:18px">Cummulative GPA: <span style="border-bottom:1px solid #000;font-weight:normal;font-size:18px">{{ round($year->cgpa,2) }}/4.00</span></td>
                      <td style="font-weight:bold;font-size:18px;text-align:right">Percentage of Marks: <span style="border-bottom:1px solid #000;font-weight:normal;font-size:18px">{{ $year->uptonowpercentage }}</span></td>
                  </tr>
                  <!-- <tr>
                      <td style="font-weight:bold;font-size:18px">Number of Credit Hours Attempted: <span style="border-bottom:1px solid #000;font-weight:normal;font-size:18px">000/000</span></td>
                      <td style="font-weight:bold;font-size:18px;text-align:right">Credit Hours Passed: <span style="border-bottom:1px solid #000;font-weight:normal;font-size:18px">000</span></td>
                  </tr> -->
                   <tr>
                      <td colspan="2" style="font-weight:bold;font-size:18px;text-align:center">Status: <span style="border-bottom:1px solid #000;font-weight:normal;font-size:18px">{{ $year->remarks }}</span></td>
                  </tr>
              </table>
              <!-- <div style="font-weight:bold;font-size:18px;border-top:1px solid #000;max-width:150px;margin-left:10%;text-align:center;margin-top:50px">Checked by</div> -->
              <div style="width:100%;margin-left:10%;margin-right:10%;">
              <!-- <table style="width:100%;">
                  <tr>
                  <td style="padding-right:10px"><div style="font-weight:bold;font-size:16px;border-top:1px solid #000;text-align:center;margin-top:50px">Superintendent (EXAMINATIONS)</div></td>

                  <td style="text-align:right;padding-left:10px"><div style="font-weight:bold;font-size:16px;border-top:1px solid #000;text-align:center;margin-top:50px">AD / DD / DIRECTOR (EXAMINATIONS)</div></td>
             </tr>
              </table> -->
              <br><br>
              </div>
                 <table style="width:100%;margin-left:20px;margin-right:20px;margin-top:20px">
                <tr>
                      <td style="font-weight:bold;font-size:18px;text-align:center">Issued on: <span style="border-bottom:1px solid #000;font-weight:normal;font-size:18px">{{ date('F d, Y') }}</span></td>
                  </tr>
              </table>
              <div style="width:100%;margin-left:10%;margin-right:10%;border:1px solid #000">
                  <div style="text-align:center;font-size:17px;font-weight:bold"><span style="border-bottom:1px solid #000;">KEY TO LETTER GRADES</span></div>
                  <div style="text-align:center;font-size:13px;padding:10px">80% and above A 75-79.99% B+ 70-74.99% B 65-69.99% C+ 60-64.99% C 55-59.99% D+ 50-54.99% D Below 50% F<!-- Below C is failure for {{ $year->acadprogname }} --></div>
              </div>
              <div style="text-align:center;font-size:13px;padding:5px">Errors and omissions are excepted. System generated result intimation. (for information only).</div>
     <footer style="width: 100%;" id="footer">
        <img src="{{ public_path('images/resultintimation-bottom.jpg') }}" alt="footer" style="width: 100%;">
    </footer>
              @php
  }
  @endphp
</body>
</html>
@php
 // exit;
@endphp
