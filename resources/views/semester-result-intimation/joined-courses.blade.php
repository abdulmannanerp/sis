@extends('adminlte::page')
@section('title', 'Permission Form')
@section('content_header')
@php
  $date = Carbon\Carbon::now();
  $date = date_format($date,'d M Y');
  $i = 0;
  if(empty($regno)){
    @endphp
    <h1 style="text-align: center;" target_blank><b>No Registration Number Found, Kindly Visit Automation Center</b></h1>
    @php
  }else{
  $currentSemester = '';
  foreach($semcode as $key=>$year)
    if ($i == 0) {
      $currentSemester = $year->semcode;
      break;
    }
     $coursecodes=array();
@endphp
  <a href="/semester-result-intimation" style="    float: right;" target="_blank" class="btn btn-primary btn-sm" role="button">Download Result Intimation</a>
  <div class="current_date"><strong>Date: @php echo $date @endphp</strong></div>

<h1 class="headingOne" style="text-align: center;font-size: 40px;"><b>INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</b></h1>
  <h1 style="text-align: center;font-size: 35px;text-transform: uppercase;font-weight: 900;"><b>Result Intimation</b></h1>
  <!-- <h1 style="text-align: center;"><b>(Course Registration / Permission Form For @php  echo $currentSemester @endphp)</b></h1> -->
  <h1 style="text-align: center;font-size: 25px;"><b>SEMESTER FALL-2023</b></h1>

  <table class="table table-hover" id="student-data" style="width:80%; margin-top: 20px;">
    <tbody>
        <tr>
            <td><strong>Registration No</strong></td>
          <td>@php
          if(!empty($year->regno)){
          echo $year->regno;
          }
          @endphp</td>
        </tr>
         <tr>
          <td><strong>Student Name:</strong></td>
          <td>{{ $year->studname }}</td>
        </tr>
         <tr>
          <td><strong>Father's Name:</strong></td>
          <td>{{ $year->studfathername }}</td>
        </tr>
        <tr>
          <td><strong>Faculty:</strong></td>
          <td>{{ $year->facname }}</td>
        </tr>
        <tr>
          <td><strong>Department:</strong></td>
          <td>{{ $year->depname }}</td>
        </tr>

        <tr>
          <td><strong>Dgree Programme</strong></td>
          <td>{{ $year->acadprogname }}</td>
          <!--
            <td><strong>Phone #</strong></td>
            <td>{{ $year->studphone }}</td>
          -->
        </tr>
         <!--
          <tr>
            <td><strong>Email:</strong></td>
            <td>{{ $year->studemail }}</td>
          </tr>
        -->
        </tr>
    </tbody>
  </table>
          @php
            $currentSemester='';
          @endphp
            @foreach($semcode as $key=>$year)
            @php
            if(in_array($year->coursecode,$coursecodes)){
            continue;
            }
            $coursecodes[]=$year->coursecode;
                if($year->semcode!='FALL-2023'){
                    continue;
                }
            @endphp
               @if(empty($currentSemester))
                @php
                $currentSemester=$year->semcode;
              @endphp
              <table class="table table-hover" style="width:80%">
                <thead class="thead-dark">
                  <tr>
                      <th style="width: 12%;">Course Code</th>
                      <th style="width: 35%;">Course Title</th>
                      <th style="width: 12%;">Cr. Hrs</th>
                      <th style="width: 12%;">Grade</th>
                  </tr>
                </thead>
                <tbody>
                     @elseif($currentSemester!=$year->semcode)
                      @php
                          $currentSemester=$year->semcode;
                       @endphp
                </tbody>
              </table>
              <div><strong>Semester </strong> {{ $year->semcode }}</div>
                 <table class="table table-hover" style="width:80%">
                    <thead class="thead-dark">
                      <tr>
                          <th style="width: 12%;">Code</th>
                          <th style="width: 35%;">Course Title</th>
                          <th style="width: 12%;">Cr. Hrs</th>
                          <th style="width: 12%;">Grade</th>

                      </tr>
                  </thead>
                  <tbody>
                         @endif
                        <tr>
                          <td>{{ $year->coursecode }}</td>
                          <td>{{ $year->coursename }}</td>
                          <td>{{ $year->credithrs }}</td>
                          <td>
                            @if($year->semcode!='FALL-2021' )
                          {{ $year->grade }}
                        @endif
                        </td>
                        </tr>
                      @endforeach
                      @php
  }
  @endphp
                      <!-- <tr>
                        <td><b>GPA:</b>gpa val</td>
                        <td><b>Total Cr.:</b>credit hour val</td>
                        <td><b>Cm%age:</b>%age val</td>
                      </tr>
                      <tr>
                        <td><b>Credit Attempted:</b>total cr hr val</td>
                        <td><b>Credit Pass:</b>pass cr hr val</td>
                      </tr> -->
                  </tbody>
              </table>
              <table id="cgpa">
                  <tr>
                      <td style="font-size: 20px;font-weight: bold;">Cummulative GPA:  <span style="font-weight: normal;border-bottom: 1px solid #000;">{{ substr($year->SEMCGPA,0,4) }}/4.00</span></td>
                  </tr>
              </table>
              <style>
                  table#student-data tr td:first-child {
    max-width: 50px !important;
}
              </style>
@stop
@section('content')

@endsection
