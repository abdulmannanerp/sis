@extends('adminlte::page')

@section('title', 'SIS | Dashboard')

@section('content_header')
    <h1>Dear {{auth()->user()->name ?? '' }}</h1>
@stop

@section('content')
	@php
	if($degreeCompleted==0){
		echo '<p>Your degree not yet been completed</p>';
	}else{
		echo '<p>Your degree has been completed</p>';
	}
	if($hifzTestPassed==0){
		echo '<p>You did not passed the hifz test yet</p>';
	}else{
		echo '<p>You have passed the hifz test.</p>';
	}
	@endphp
    <p><strong>Note:</strong><br>
	Please download and pay the degree clearance fee before initializing the clearance application.
	</p>
	<button class="btn btn-primary">Download clearance fee challan</button>
@stop