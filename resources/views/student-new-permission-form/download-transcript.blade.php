<!DOCTYPE html >
    <html lang="en">
        <head>
            <meta charset="UTF-8">
                <title>Transcript</title>
                <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
                    <link href="{{public_path('permission-form/download-permission-form.css')}}" rel="stylesheet" type="text/css" media="all">
                        <style>
                            @media print {
                                @page {size: [portrait]; }
}
                            .container {
                                /* width: 100%; */
                                margin: 0 auto;
                            font-family: 'Open Sans', sans-serif;
}
html {
    font-size: 12px !important;
}
.qr_trans{
    width: 12%;
    position: absolute;
    right: -10px;
    top: -10px;
}
                            .date-div{
                                text - align: center;
                            float:right;
}
                            .headingOne{
                                clear: both;
                            text-align: center;
                            font-size:15px;
}
                            .headingTwo{
                                text - align: center;
                            font-size:14px;
                            text-decoration: underline;
}
                            .no-border-table{
                                width:100%;
}
                            .no-border-table td{
                                border: 0px;
                            font-size:12px;
}
                            .no-border-table th{
                                border: 0px;
                            font-size:12px;
}
                            .no-border-table strong{
                                border: 0px;
                            font-size:12px;
}
                            .smooth-border{
                                border:2px solid #000000;
                            margin-top:5px;
}
                            .perm-tab th{
                                border: 0.5px solid #333;
                            text-align: left;
                            /*padding: 2px;*/
}
                            .perm-tab td{
                                border: 0.5px solid #333;
                            text-align: left;
                            /*padding: 2px;*/
}
                            table td{
                                border: 0px;
                            /*padding: 2px;*/
}
                            table {
                                border - collapse: collapse;
                            align:"center"
}
tr{
    padding: 0px !important;
    margin: 0px !important;
}
.first_tr td, .second_tr td, .third_tr td, .forth_tr td, .fifth_tr td, .sixth_tr td {
    font-size: 14px;
    padding: 10px 0px 10px 0px;
}
.forth_tr td{
    text-align: center;
}
.forth_tr td {
    border: 0.5px solid #000;
}
.sixth_tr td {
    border: 0.5px solid #000;
}
.third_tr td {
    padding-top: 40px;
}
.text_line {
    border-top: 1px solid #000;
}
.txt-rgt{
    text-align: right;
}
                            th, td, strong {
                                font - size: 8px;
}
                            div, strong {
                                font - size: 8px;
}
                            .inner-tr-cls td:nth-child(1), .inner-tr-cls th:nth-child(1) {
                                width: 40px;
                            min-width: 40px;
                            max-width: 40px;
}
                            .inner-tr-cls td:nth-child(2), .inner-tr-cls th:nth-child(2) {
                                width: 150px;
                            min-width: 150px;
                            max-width: 150px;
}
                            .inner-tr-cls td:nth-child(3), .inner-tr-cls th:nth-child(3)  {
                                width: 40px;
                            min-width: 40px;
                            max-width: 40px;
}
                            .inner-tr-cls td:nth-child(4), .inner-tr-cls th:nth-child(4)  {
                                width: 40px;
                            min-width: 40px;
                            max-width: 40px;
}
                            .inner-tr-cls td:nth-child(5), .inner-tr-cls th:nth-child(5) {
                                width: 40px;
                            min-width: 40px;
                            max-width: 40px;
}
                            .inner-tr-cls td:nth-child(6), .inner-tr-cls th:nth-child(6)  {
                                width: 150px;
                            min-width: 150px;
                            max-width: 150px;
}
                            .inner-tr-cls td:nth-child(7), .inner-tr-cls th:nth-child(7)  {
                                width: 40px;
                            min-width: 40px;
                            max-width: 40px;
}
                            .inner-tr-cls td:nth-child(8), .inner-tr-cls th:nth-child(8)  {
                                width: 40px;
                            min-width: 40px;
                            max-width: 40px;
}
                        </style>
                    </head>
                    @php
                    $date = Carbon\Carbon::now();
                    $date = date_format($date,'d M Y');

                    $i = 0;
                    if(!empty($regno)){
                        @endphp
                        <h1><b>No Registration Number Found, Kindly Visit Automation Center</b></h1>
                @php
  }else{
                    $currentSemester = '';
                $semester_courses=array();
  foreach($semcode as $key=>$year){
                    $semester_courses[$year -> semcode][] = $year;
                if(empty($currentSemester)){
                    $currentSemester = $year -> semcode;
    }
  }
                $currentcourses=false;
                if(!empty($semester_courses['FALL-2023'])){
                    $currentcourses = $semester_courses['FALL-2023'];
                unset($semester_courses['FALL-2023']);
  }
                @endphp

                <div style="text-align: right; margin-top: 50px;">
                    <img src="data:image/png;base64,<?= $qrTranscript ?>" alt="QR Code" class="qr_trans">
                </div>
                <!-- Top Table Start -->
                <table class="no-border-table">
                    <tbody>
                        <tr>
                            <td><strong>Registration Number</strong> {{ $year-> regno}}</td>
                            <td><strong>Roll No:</strong> </td>
                        </tr>
                        <tr>
                            <td><strong>Student Name:</strong> {{ $year-> studname}} </td>
                            <td><strong>Registered In:</strong> </td>
                        </tr>
                        <tr>
                            <td><strong>Father Name:</strong> {{ $year-> studfathername}}</td>
                            <td><strong>Completed In:</strong> </td>
                        </tr>
                        <tr>
                            <td><strong>Faculty: </strong> {{ $year-> facname}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><strong>Programme: </strong> {{ $year-> acadprogname}}</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <!-- Top Table End -->
                <div class="smooth-border"></div>
                <!--<div><strong>Semester Results </strong></div>-->
                @php
                $index=0;
                $overallPercentage = '';
                $cgpaInn = '';
                @endphp
                <div style="width:100%;display:inline-block">
                    <table class="perm-tab" style="width:100%;">
                        <thead>
                            <tr class="inner-tr-cls">
                                <th>Code</th>
                                <th>Course Title</th>
                                <th>Cr Hrs</th>
                                <th>Grd</th>

                                <th>Code</th>
                                <th>Course Title</th>
                                <th>Cr Hrs</th>
                                <th>Grd</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $creditAttempt=0; @endphp
                            @php $counter = 0; @endphp
                            @foreach($semester_courses as $key=>$semester_course)
                                      
                                      @foreach($semester_course as $skey=>$sem_course)
                                        @if($counter % 2 == 0)
                                        <tr class="inner-tr-cls">
                                            @endif

                                            <td>{{ $sem_course-> coursecode}}</td>
                                            <td>{{ $sem_course-> coursename}}</td>
                                            <td>{{ $sem_course-> credithrs}}</td>
                                            <td>{{ $sem_course-> grade}}</td>

                                            @if($counter % 2 != 0)
                                        </tr>
                                        @endif
                                        @php $counter++; @endphp
                                      @endforeach
                              @php
                              // echo '<b>GPA:</b>&nbsp;'. bcdiv($sem_course->gpa, 1, 2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Total Cr:</b>&nbsp;'.$sem_course->uptonowcrdhratt.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Cm%age:</b>&nbsp;'. bcdiv($sem_course->uptonowpercentage, 1, 2).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$sem_course->remarks;
                              $index++;
                              $creditAttempt = $sem_course->credhrattempt;
                              $overallPercentage = bcdiv($sem_course->overallpercentage, 1, 2);
                              $cgpaInn = bcdiv($sem_course->cgpa, 1, 2);
                              @endphp
                            @endforeach
                        </tbody>
                    </table>
                    @php
                    // echo '<br /><br /><b>Overall %age:</b>&nbsp;'.$overallPercentage.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>CGPA:</b>&nbsp;'.$cgpaInn.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Credit Attempt:</b>&nbsp;'.$creditAttempt;
                    @endphp
                </div>
                <div style="width:100%;display:inline-block; clear: both">

                    <table style="width:100%;">
                        <tbody>
                        <tr class="first_tr">
                        <td colspan="3">Total Credit Hours: </td>
                        <td colspan="3">Cumulative G.P.A: </td>
                        <td colspan="3">Percentage Of Marks:</td>
                        </tr>
                        <tr class="second_tr">
                        <td colspan="9"> The student named above cannot be considered to have obtained the degree unless he satisfies the prescribed oral Hifz Test.</td>
                        </tr>
                        <tr class="third_tr">
                        <td colspan="3"><span class="text_line">Addl. Director (Examinations)</span></td>
                        <td colspan="3">Issued on: </td>
                        <td colspan="3"><span class="text_line">Director (Academics & Examinations)</span></td>
                        </tr>
                        <tr class="forth_tr">
                        <td colspan="9"><b>KEY TO LETTER GRADES</b></td>
                        </tr>
                        <tr class="sixth_tr">
                        <td><b>Grading Scale:</b></td>
                        <td>80% and above A</td>
                        <td>75-79.99% B+</td>
                        <td>70-74.99% B</td>
                        <td>65-69.99% C+</td>
                        <td>60-64.99% C</td>
                        <td>55-59.99% D+</td>
                        <td>50-54.99% D</td>
                        <td>Below 50% F</td>
                        </tr>
                        <tr class="seventh_tr">
                        <td colspan="5">Errors and Omissions Excepted</td>
                        <td colspan="4" class="txt-rgt">(Computer Generated Transcript)</td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                @php
}
                @endphp

            </body>
    </html>
@php
// exit;
@endphp
