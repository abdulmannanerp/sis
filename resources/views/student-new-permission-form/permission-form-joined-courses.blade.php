@extends('adminlte::page')
@section('title', 'Permission Form')
@section('content_header')
@php
  $date = Carbon\Carbon::now();
  $date = date_format($date,'d M Y');
  $i = 0;
  if(empty($regno)){
    @endphp
    <h1 style="text-align: center;" target_blank><b>No Registration Number Found, Kindly Visit Automation Center</b></h1>
    @php
  }else{
  $currentSemester = '';
  foreach($semcode as $key=>$year)
    if ($i == 0) {
      $currentSemester = $year->semcode;
      break;
    }
@endphp
  <!-- <a href="download-permission-form-pdf" target="_blank" class="btn btn-primary btn-sm" role="button">Download Permission Form</a> -->
  <div class="current_date"><strong>Date: @php echo $date @endphp</strong></div>
  <h1 style="text-align: center;"><b>INTERNATIONAL ISLAMIC UNIVERSITY, ISLAMABAD</b></h1>
  <!-- <h1 style="text-align: center;"><b>(Course Registration / Permission Form For @php  echo $currentSemester @endphp)</b></h1> -->
  <h1 style="text-align: center;"><b>Registered Courses For @php  echo $currentSemester @endphp)</b></h1>
  <table class="table table-hover" style="width:80%; margin-top: 20px;">
    <tbody>
        <tr>
          <td><strong>Registration Number</strong></td>
          <td>@php
          if(!empty($year->regno)){
          echo $year->regno;
          }
          @endphp</td>
          <td><strong>Faculty:</strong></td>
          <td>{{ $year->facname }}</td>
        </tr>
        <tr>
          <td><strong>Student Name:</strong></td>
          <td>{{ $year->studname }}</td>
          <td><strong>Department:</strong></td>
          <td>{{ $year->depname }}</td>
        </tr>
        <tr>
          <td><strong>Father Name:</strong></td>
          <td>{{ $year->studfathername }}</td>
          <td><strong>Batch:</strong></td>
          <td>{{ $year->batchname }}</td>
        </tr>
        <tr>
          <td><strong>Programme</strong></td>
          <td>{{ $year->acadprogname }}</td>
          <!--
            <td><strong>Phone #</strong></td>
            <td>{{ $year->studphone }}</td>
          -->
        </tr>
         <!--
          <tr>
            <td><strong>Email:</strong></td>
            <td>{{ $year->studemail }}</td>
          </tr>
        -->
        </tr>
    </tbody>
  </table>
    <h1 style="text-align: center;"><b>Exam History</b></h1>
          @php
            $currentSemester='';
          @endphp
            @foreach($semcode as $key=>$year)
               @if(empty($currentSemester))
                @php
                $currentSemester=$year->semcode;
              @endphp
               <div><h3>Semester Results</h3></div>
              <div><strong>Semester </strong> {{ $year->semcode }}  </div>
              <table class="table table-hover" style="width:80%">
                <thead class="thead-dark">
                  <tr>
                      <th style="width: 12%;">Code</th>
                      <th style="width: 35%;">Course Title</th>
                      <th style="width: 12%;">Grade</th>
                      <th style="width: 12%;">Credit Hours</th>
                  </tr>
                </thead>
                <tbody>
                     @elseif($currentSemester!=$year->semcode)
                      @php
                          $currentSemester=$year->semcode;
                       @endphp
                </tbody>
              </table>
              <div><strong>Semester </strong> {{ $year->semcode }}</div>
                 <table class="table table-hover" style="width:80%">
                    <thead class="thead-dark">
                      <tr>
                          <th style="width: 12%;">Code</th>
                          <th style="width: 35%;">Course Title</th>
                          <th style="width: 12%;">Grade</th>
                          <th style="width: 12%;">Credit Hours</th>
                      </tr>
                  </thead>
                  <tbody>
                         @endif
                        <tr>
                          <td>{{ $year->coursecode }}</td>
                          <td>{{ $year->coursename }}</td>
                          <td>
                            @if($year->semcode!='FALL-2021' )
                          {{ $year->grade }}
                        @endif
                        </td>
                          <td>{{ $year->credithrs }}</td>
                        </tr>
                      @endforeach
                      @php
  }
  @endphp
                      <!-- <tr>
                        <td><b>GPA:</b>gpa val</td>
                        <td><b>Total Cr.:</b>credit hour val</td>
                        <td><b>Cm%age:</b>%age val</td>
                      </tr>
                      <tr>
                        <td><b>Credit Attempted:</b>total cr hr val</td>
                        <td><b>Credit Pass:</b>pass cr hr val</td>
                      </tr> -->
                  </tbody>
              </table>
@stop
@section('content')
  
@endsection