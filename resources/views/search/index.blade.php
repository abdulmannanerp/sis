@extends('voyager::master')

@section('page_title', 'Search')

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-search"></i> Search
    </h1>
</div>
@stop

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <form action="{{ route('search') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Enter search query</label>
                            <input type="text" name="query" class="form-control" placeholder="Search Query" required>
                        </div>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="searchBy" value="regno" required>
                                <label class="form-check-label">Registration Number</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="searchBy" value="challan" required>
                                <label class="form-check-label">Challan Number</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="searchBy" value="email" required>
                                <label class="form-check-label">IIU Email</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Search" class="btn btn-primary">
                        </div>
                    </form>
                    @if ( $response )
                        <h3> Result </h3>
                        @foreach ( $response as $key => $value )
                            @if(strtoupper($key) == 'REGNO')
                                @php 
                                $reg_no_q = $value;
                                @endphp
                            
                            @endif
                        @endforeach
                        <div style="text-align: right;">
                            <form action="{{ route('generateQr') }}" target="_blank" method="POST">
                                @csrf
                                <input type="hidden" name="regno" value="{{$reg_no_q}}">
                                <button target="_blank" type="submit" class="btn btn-primary">Generate QR</button>
                            </form>
                        </div>
                        <table class="table table-stripped">
                            <tbody>
                                @foreach ( $response as $key => $value )
                                    
                                    @if(!is_array($value))
                                        <tr>
                                            <td>{{ strtoupper($key) }} </td>
                                            <td>{{ $value }} </td>
                                        </tr>
                                    @else
                                        {{-- This condition is needed in case of challan becuase paychallandetail is eager loaded with paychallan --}}
                                        @foreach ($value as $key => $value )
                                            <tr>
                                                <td> FEE CODE</td>
                                                <td>{{ $value->feecode }} </td>
                                            </tr>
                                            <tr>
                                                <td> FEE AMOUNT</td>
                                                <td>{{ $value->feeamnt }} </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop