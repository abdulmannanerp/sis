@extends('voyager::master')

@section('page_title', 'Modify Challan')

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-refresh"></i> 
        Modify Challan
    </h1>
</div>
@stop

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <form action="#" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="challanNumber">Enter Challan Number</label>
                            <input type="text" name="challanNumber" value="{{ $challanNumber }}" class="form-control" requied>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Get Challan" class="btn btn-primary">
                        </div>
                    </form>
                    @if ($challanDetails)
                        <form>
                            <div class="form-group">
                                <label>Reg Number</label>
                                <input type="text" name="regno" value="{{$challanDetails->regno}}" readonly>    
                            </div>
                            <div class="form-group">
                                <label>Semester</label>
                                <input type="text" name="semcode" value="{{$challanDetails->semcode}}" readonly>    
                            </div>
                            <div class="form-group">
                                <label>Due Date</label>
                                <input type="date" name="challanduedate" value="{{date_format(date_create($challanDetails->challanduedate), 'Y-m-d')}}">    
                            </div>
                            <div class="form-group">
                                <label>Challan Amount</label>
                                <input type="text" name="challanamnt" value="{{$challanDetails->challanamnt}}">    
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Update Challan">
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop