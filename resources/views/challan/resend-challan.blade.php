@extends('voyager::master')

@section('page_title', 'Resend Fee Challan')

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-refresh"></i> 
        Resend Fee Challan
    </h1>
</div>
@stop

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <form action="{{ route('resendChallan') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="date">Select Date</label>
                            <input type="date" name="date" value="{{old('date')}}" required class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="date">Select Bank</label>
                            <select name="bankName" class="form-control" required>
                                <option value="">--Select--</option>
                                <option value="abl" {{(old('bankName') == 'abl')? 'Selected' : ''}} >Allied Bank Limited</option>
                                <option value="fwbl" {{(old('bankName') == 'fwbl')? 'Selected' : ''}}>First Women Bank Limited</option>
                                <option value="hbl" {{(old('bankName') == 'hbl')? 'Selected' : ''}}>Habib Bank Limited</option>
                                <option value="fbl" {{(old('bankName') == 'fbl')? 'Selected' : ''}}>Faysal Bank Limited</option>
                                <option value="alfalah" {{(old('bankName') == 'alfalah')? 'Selected' : ''}}>Alfalah Bank Limited</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Get Challan" class="btn btn-primary">
                        </div>
                    </form>
                    @if($csvPath)
                        <hr>
                        <form action="{{ route('downloadCsvFile') }}" method="POST">
                            @csrf
                            <input type="hidden" name="path" value="{{ $csvPath }}">
                            <button type="submit" class="btn btn-primary">Download File</button>
                        </form>

                        <form action="{{ route('emailSingleDayChallansToBank') }}" method="POST">
                            @csrf
                            <input type="hidden" name="path" value="{{ $csvPath }}">
                            <input type="hidden" name="bank" value="{{ (!empty($bank))? $bank :  '' }}">
                            <button type="submit" class="btn btn-primary">Send Email</button>
                        </form>
                    @else
                        <div class="alert alert-warning">
                            No Challans Found
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop