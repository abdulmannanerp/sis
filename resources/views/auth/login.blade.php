@extends('layout.app')
@section('styles')
<style>
    body {
        margin-top: 5em;
    }
    .logo {
        height: 150px;
        width: 150px;
        display: block;
        margin: 0 auto;
    }

    .primary-color {
        background-color: #216821;
        color: #fff;
    }
    .support-color {
        background-color: #23216a;
        color: #fff;
    }
    
    .login-with-iiu-btn {
        text-decoration: none;
    }

    .card-footer a {
        color: #fff;
    }
    .separator {
            /* margin: 20px 0; */
            margin: 0px 0;
    }
    .other-button {
            background-color: #ccc;
            color: #333;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3 col-sm-12">
            @if ($error)
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            @endif
            <div class="card mt-3">
                <div class="card-body">
                    <div class="text-center">
                        <img src="/images/logo.jpg" class="img-responsive logo" alt="IIU Logo">
                        <a href="/redirect" class="btn btn-sm btn-dark login-with-iiu-btn my-3">
                            Login with IIU email
                        </a>
                        <div class="separator">OR</div>
                        <a href="{{ route('loginWithCnic') }}" class="btn btn-sm support-color login-with-iiu-btn my-3">Login with your CNIC</a>
                    </div>
                </div>
                <div class="card-footer primary-color text-center">
                    <small>All those students who got admission in Spring 2024 can download their challans from <a href="https://cms.iiu.edu.pk/">https://cms.iiu.edu.pk/</a></small>
                </div>
                <div class="card-footer primary-color text-center">
                    <small>Please contact <a href="mailto:portal@iiu.edu.pk?subject=SIS Portal Login Issue">portal@iiu.edu.pk</a> if you have any trouble signing in to your SIS Portal</small>
                </div>
                <div class="card-footer support-color text-center">
                    <small>Please contact <a href="mailto:support@iiu.edu.pk?subject=IIUI Email Issue">support@iiu.edu.pk</a> if you have any trouble signing in to your IIUI Email Account</small>
                </div>
            </div>
            <div class="no-email-address-container text-right">
                <small>
                    <a href="{{ route('loginWithCnic') }}">Don't have IIU email address?</a>
                </small>
            </div>
        </div>
    </div>
</div>
@endsection