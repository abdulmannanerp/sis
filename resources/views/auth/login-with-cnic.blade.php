@extends('layout.app')
@section('styles')
<style>
    body {
        margin-top: 5em;
    }
    .logo {
        height: 150px;
        width: 150px;
        display: block;
        margin: 0 auto;
    }

    .primary-color {
        background-color: #216821;
        color: #fff;
    }
    .support-color {
        background-color: #23216a;
        color: #fff;
    }
    
    .login-with-iiu-btn {
        text-decoration: none;
    }

    .card-footer a {
        color: #fff;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3 col-sm-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p class="m-0"> {{ $error }} </p>
                    @endforeach    
                </div>
            @endif
            @if ($error)
                <p class="alert alert-danger">
                    {{ $error }}
                </p>
            @endif
            <div class="card mt-3">
                <div class="card-body">
                    <form action="{{ route('loginWithCnic') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">Enter First Name</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>

                        <div class="form-group">
                            <label for="#">Enter CNIC/Passport Number (With Dashes)</label>
                            <input type="text" class="form-control" name="cnic" placeholder="xxxxx-xxxxxxx-x" required>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-dark">Login</button>
                        </div>
                    </form>
                </div>
                <div class="card-footer primary-color text-center">
                    <small>All those students who got admission in Spring 2024 can download their challans from <a href="https://cms.iiu.edu.pk/">https://cms.iiu.edu.pk/</a></small>
                </div>
                <div class="card-footer primary-color text-center">
                    <small>Please contact <a href="mailto:portal@iiu.edu.pk?subject=SIS Portal Login Issue">portal@iiu.edu.pk</a> if you have any trouble signing in to your SIS Portal</small>
                </div>
                <div class="card-footer support-color text-center">
                    <small>Please contact <a href="mailto:support@iiu.edu.pk?subject=IIUI Email Issue">support@iiu.edu.pk</a> if you have any trouble signing in to your IIUI Email Account</small>
                </div>
            </div>
            <div class="no-email-address-container text-right">
                <small>
                    <a href="{{ route('login') }}">Login with IIU Email?</a>
                </small>
            </div>
        </div>
    </div>
</div>
@endsection