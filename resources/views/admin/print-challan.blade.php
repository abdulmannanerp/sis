@extends('voyager::master')

@section('page_title', 'Print Challan')

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-credit-cards"></i> Print Challan
    </h1>
</div>
@stop

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <form action="{{ route('adminPrintChallan') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Enter Registration Number</label>
                            <input type="text" name="regno" class="form-control" placeholder="208-FU/MSHS/F17" required>
                        </div>
                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input adjust-fine" name="adjust-fine">
                            <label class="form-check-label">Adjust Fine</label>
                        </div>
                        <div class="form-group hidden">
                            <label>Enter Fine</label>
                            <input type="text" name="fine" class="form-control" placeholder="600">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Print Challan" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
    <script>
        $(function(){
            $('.adjust-fine').on('change', function(){
                let $this = $(this),
                    fineContainer = $this.parent().next();
                if ( $this.is(':checked')) {
                    fineContainer
                        .removeClass('hidden')
                        .find(':input')
                        .eq(0)
                        .attr('required', 'required');
                    return;
                }
                fineContainer
                    .addClass('hidden')
                    .find(':input')
                    .eq(0)
                    .removeAttr('required');
            });
            $('.adjust-fine').trigger('change');
        });
    </script>
@endsection