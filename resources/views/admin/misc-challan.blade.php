@extends('voyager::master')

@section('page_title', 'Misc Challan')

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-credit-cards"></i> Misc Challan
    </h1>
</div>
@endsection

@section('content')
<div class="page-content browse container-fluid">
    @include('voyager::alerts')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="resync-contaier">
                    <a href="{{ route('get-fee-codes') }}" target="blank" style="margin-right:10px" class="btn btn-sm btn-primary float-right d-inline-block">Resync Codes</a>
                </div>
                <div class="panel-body">
                    <form action="{{ route('adminGenerateMiscChallan') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Enter Registration Number</label>
                            <input type="text" name="regno" class="form-control" placeholder="As per student card (208-FU/MSHS/F17)" required>
                        </div>
                        <div class="form-group">
                            <label for="">Select Challan</label>
                            <select name="fee" class="form-control select2">
                                @foreach ( $challans as $challan )
                                    <option value="{{ $challan->feecode }}">{{ $challan->feedesc }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="#">Fee Amount</label>
                            <input type="text" class="form-control" name="amount">
                            <small class="text-muted">Leave empty for default amount</small>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Print Challan" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection