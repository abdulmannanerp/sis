<?php
Route::get('/', 'AuthenticationController@index')->name('login');
Route::match(['get', 'post'], '/login-with-cnic', 'AuthenticationController@loginWithCnic')
    ->name('loginWithCnic');
Route::get('/redirect', 'AuthenticationController@redirectToProvider');
Route::get('/callback', 'AuthenticationController@handleProviderCallback')->name('callback');
Route::post('/logout', 'AuthenticationController@logout')->name('logout');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::match(['get', 'post'], '/search', 'SearchController@index')
        ->name('search');
    Route::match(['get', 'post'], '/print-challan', 'FeeChallanController@adminPrintChallan')
        ->name('adminPrintChallan');
    /* ********************************************************************************************************** */
    //                                             RESEND CHALLAN START
    /* ********************************************************************************************************** */
    Route::match(['get','post'], '/resend-fee-challan', 'ChallanController@resendChallan')
        ->name('resendChallan');
    /* ********************************************************************************************************** */
    //                                                  END
    /* ********************************************************************************************************** */
    Route::post('/download-csv/file', 'ChallanController@downloadCsvFile')
        ->name('downloadCsvFile');
    Route::post('/email-single-day-challan', 'ChallanController@emailSingleDayChallansToBank')
        ->name('emailSingleDayChallansToBank');
    Route::match(['get', 'post'], '/verify', 'FeeVerificationController@index')
        ->name('feeVerify');
    Route::post('/verify-challans', 'FeeVerificationController@verifyChallans')
        ->name('verifyChallans');

    Route::match(['get', 'post'], '/block', 'FeeController@index')
        ->name('blockFee');
    Route::get('/save-challan', 'FeeController@saveChallan')
        ->name('saveChallan');
    Route::match(['get', 'post'], '/challan/modify', 'ChallanController@modifyChallan');
    Route::get('authenticate/{userId}', 'UtilityController@authenticate')
        ->name('authenticte');
    // Route::post('/build-selected-date-challan', 'ChallanController@buldSelectedDateChallan')
    //     ->name('buldSelectedDateChallan');

    Route::namespace('Admin')->group(function(){
        Route::match(['get', 'post'], '/generate-misc-challan', 'MiscChallanController@index')
            ->name('adminGenerateMiscChallan');
    });

});
// START - Route::middleware
Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/fee-challan', 'FeeChallanController@index');
    Route::get(
        '/fee-challan/download/{id?}',
        'FeeChallanController@exportPdf'
    )->name('downloadFeeChallan');

/* *************************************************************************************** */
//                              Student Registered Courses
//                             Routes->Controller->Function
/* *************************************************************************************** */
Route::get('/my-courses', 'PermissionForm@getExistingCourses')->name('studentCoursesRegisteration');

// Route::get('/download-permission-form-pdf', 'PermissionForm@downloadPermissionFormPDF')->name('downloadPermissionFormPDF');


// Route::get('/semester-result-intimation', 'ResultIntimation@downloadResultIntimationPDF')->name('downloadResultIntimationPDF');

Route::match(['get','post'], '/semester-result-intimation', 'ResultIntimation@downloadResultIntimationPDF')->name('downloadResultIntimationPDF');
/* *************************************************************************************** */

});
// END - Route::middleware

Route::get('/build-challan-csv', 'ChallanController@index')->name('buildCsv');
Route::get('/send-current-date-challan', 'ChallanController@sendCurrentDateChallan');
Route::prefix('util')->group(function () {
    Route::get('/get-fee-codes', 'UtilityController@getFeeCodesFromAljamia')
        ->name('get-fee-codes');
});
/* Misc Challan Route **///'
Route::middleware(['auth'])->group(function () {
    // Route::get('/mc-challan', 'MiscChallanController@index');
    // Route::post('/generate-Challan', 'MiscChallanController@generateChallan')->name('generateChallan');
    // Route::post('/viewmc-challan', 'MiscChallanController@viewMiscChallan')->name('viewMisChallan');
    Route::get('/mc-challan', 'MiscChallanController@index');
    Route::match(['GET', 'POST'], '/viewmc-challan', 'MiscChallanController@viewMiscChallan')
        ->name('viewMisChallan');
    Route::post('/generate-Challan', 'MiscChallanController@generateChallan')
        ->name('generateChallan');

        Route::get('/permission-form', 'PermissionForm@getPermissionForm')->name('permission-form');
        Route::get('/download-permission-form', 'PermissionForm@downloadPermissionForm')->name('download-permission-form');

        Route::get('/download-transcript', 'PermissionForm@downloadTranscript')->name('download-transcript');
});

/* Exam Route **///'
Route::middleware(['auth'])->group(function () {
    Route::get('/rollno-slip', 'ExamController@index');
    Route::get(
        '/exam-slip/download/{id?}',
        'ExamController@exportPdf'
    )->name('downloadExamSlip');
});
Route::get('/emailrollnoslip', 'ExamController@emailrollnoslip')->name('emailrollnoslip');
Route::get('/Sentemailrollnoslip', 'ExamController@Sentemailrollnoslip')->name('Sentemailrollnoslip');
Route::get('/rollnoslips', 'ExamController@rollnoslips')->name('rollnoslips');
Route::any('/getslipall', 'ExamController@getslipall')->name('getslipall');
Route::get('/update-regno', 'TestController@index');
Route::get('/aljamia-resync/{model}', 'AljamiaResyncController@index')
    ->name('resync');

/**
 * Ajax Requests
 */
Route::post('/batch', 'AjaxController@getBatches');

Route::get('/testBridge', 'TestController@testBridge');

Route::get('/transcript/{regno?}', 'TestController@exportTranscript')->name('downloadTranscript');


Route::get('/test-qr', 'TestController@testQr')->name('test-qr');

Route::match(['get', 'post'], '/generateQr', 'TestController@generateQr')->name('generateQr');