<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         $this->register();

\Gate::define('emailrollnoslip', function ($user) {
    if ($user->email == 'muhammad.talha@iiu.edu.pk') {
        return true;
    }
    return false;
});
    }
}
