<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FineAdjustmentLog extends Model
{
    protected $table = 'fine_adjustment_log';
    protected $guarded = [];
}
