<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiscChallan extends Model
{
    protected $table  = 'miscchallansetting';
    protected $guarded = [];
    public $timestamps = false;
}
