<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QrCodes extends Model
{
    protected $table  = 'qr_codes';
    protected $guarded = [];
    public $timestamps = false;
}
