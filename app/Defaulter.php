<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Defaulter extends Model
{
    protected $table = 'defaulter_fees_stud';
    protected $guarded = [];
}
