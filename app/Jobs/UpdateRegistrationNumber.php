<?php

namespace App\Jobs;

use App\User;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateRegistrationNumber implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $regno;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($regno)
    {
        $this->regno = $regno;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $client = new Client([
                'base_uri' => '111.68.97.170:64464'
            ]);

            $request = $client->request('POST', '/get-student-by-regno', [
                'json' => [ 'regno' => $this->regno ]
            ]);
            $response = json_decode((string)$request->getBody());
            if ( $response->studentiiuiemail ) {
                $user = User::where('regno', $this->regno)->first();
                $user->email = $response->studentiiuiemail;
                $user->save();
                return;
            }
            info ( 'use iiu email not found ' . $this->regno . ' ----------- ');
        } catch ( Exception $e ) {
            info ( $this->regno);
            info ('----------');
        }
    }
}
