<?php

namespace App\Http\Controllers;

use App\Feecodes;
use App\MiscChallan;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDF;

class MiscChallanController extends Controller
{
    public function index()
    {
        $gender = auth()->user()->gender;
        $feeCodes = MiscChallan::whereNotNull('feeamount')->where('feeamount', '!=', 0)->get();
        return view('student.misc-challantype', [
            'feeCodes' => $feeCodes
        ]);
    }

    /**
     * Request aljmaia for a challan
     */
    public function getFeeChallan($regno)
    {
        $challanDetails = '';
        try {
            $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
            ]);
            $response = $client->request('POST', 'get-challan-information', [
                'json' => [ 'regno' => $regno ]
            ]);
            $challanDetails = (string)$response->getBody();
        } catch (RequestException $e) {
            info($e);
            $challanDetails = '';
        }
        return $challanDetails;
    }

    /**
    * Build pdf for a challan
    */
    public function exportPdf($id = null)
    {
        $regNo = auth()->user()->regno;
        $challanDetails = json_decode($this->getFeeChallan($regNo));
        $copies = ['Account Copy', 'Bank Copy', 'Academic Copy', 'Student Copy'];
        $pdf = PDF::loadView('student.download-challan', compact('challanDetails', 'copies'));
        return $pdf->setPaper('a4', 'landscape')->stream();
    }

    public function exportPdfAdminView()
    {
        return view('admin-download-challan');
    }
    /**
     * Download challan, Temp solution
     */
    public function exportPdfAdmin()
    {
        $regNo = request('regno');
        $challanDetails = json_decode($this->getFeeChallan($regNo));
        $copies = ['Account Copy', 'Bank Copy', 'Academic Copy', 'Student Copy'];
        $pdf = PDF::loadView('student.download-challan', compact('challanDetails', 'copies'));
        return $pdf->setPaper('a4', 'landscape')->stream();
    }
    
    public function generateChallan(Request $request)
    {
        $feecode = request('feecode');
        $validator = Validator::make($request->all(), [
            'feecode' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return redirect('mc-challan')
                ->withErrors($validator)
                ->withInput();
        }
        $challanDetails = '';
        $feedetails = MiscChallan::where('feecode', $feecode)->first();
        try {
            $client = new Client([
                // 'base_uri' => 'http://usis.iiu.edu.pk:64453/fees/'
                'base_uri' => env('BRIDGE_SERVER_URI', 'http://111.68.97.170:64464/')
            ]);
            $response = $client->request('POST', 'save-misc-challan', [
                'json' => [
                    'regno' => auth()->user()->regno,
                    'gender' => (auth()->user()->gender == 'Male') ? 'M' : 'F',
                    'miscinfo' => $feedetails 
                ]
            ]);
            // die($response);
            $challanDetails = json_decode((string)$response->getBody());
        } catch (RequestException $e) {
            info($e);
        }
        if ($challanDetails) {
            $copies = ['Bank Copy', 'Account Copy',  'Academic Copy', 'Student Copy'];
            $pdf = PDF::loadView('student.static-challan', compact('challanDetails', 'copies', 'feedetails'));
            return $pdf->setPaper('a4', 'landscape')->stream();    
        }
        return back()->withErrors('Unable to generate challan');
    }
    
    public function viewMiscChallan(Request $request)
    {
        $feecode = request('feecode');
        $validator = Validator::make($request->all(), [
            'feecode' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return redirect('mc-challan')
                ->withErrors($validator)
                ->withInput();
        }
        $regNo = auth()->user()->regno;
        // $challanDetails = json_decode($this->getFeeChallan($regNo));
        $feeDetails = MiscChallan::where('feecode', $feecode)->first();
        // die($feeDetails);

        // $feedetails = DB::table('miscchallansetting')
        //     ->where('feecode', $feecode)
        //     ->first();
        return view('student.ViewmiscChallan', [
            'feedetails' => $feeDetails,
            // 'challanDetails' => $challanDetails
        ]);
    }
}
