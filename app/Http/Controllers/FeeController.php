<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\Models\Semester;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class FeeController extends Controller
{
	/**
	 * Generate block fee view
	 */
    public function index()
    {
    	$programs = Program::all();
        $semesters = Semester::orderBy('semstartdate', 'desc')->get();
    	if ( request()->method() == 'POST') {
    		return $this->viewStudents((object)request()->all());
    	}
    	return view('fee.block', [
    		'programs' => $programs,
            'semesters' => $semesters
    	]);
    }

    public function viewStudents($request)
    {
    	$response = $this->getStudentsByBatch($request->batch);
    	if (!$response) {
    		return back()->with('error', 'Unable to get student list from aljamia');	
    	}
    	return view('fee.list', [
			'students' => $response,
			'request' => $request
		]);
    }

    public function getStudentsByBatch($batch)
    {
    	$client = new Client([
    		'base_uri' => env('BRIDGE_SERVER_URI')
    	]);
    	$request = $client->request('POST', '/get-students-by-batch', [
    		'json' => [
    			'batch' => $batch
    		]
    	]);
    	return $response = json_decode((string)$request->getBody());
    }

    public function saveChallan()
    {
    	return redirect()->route('blockFee')->with('success', 'Challans Generated Successfully');
    }
}
