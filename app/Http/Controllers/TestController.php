<?php

namespace App\Http\Controllers;

use App\Jobs\UpdateRegistrationNumber;
use App\User;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Crypt;
use GuzzleHttp\Exception\RequestException;
use App\QrCodes;
use PDF;
// use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TestController extends Controller
{
    public function index()
    {
    	$users = User::all();
    	$registrationNumbersToUpdate = [];
    	foreach ( $users as $user ) {
    		$userName = explode(' ', $user->name);
    		$userName = trim($userName[count($userName) - 1]);
    		if ( $userName != $user->regno) {
    			$registrationNumbersToUpdate[] = $user->regno;
    		}
    	}
    	$registrationNumbersToUpdate = array_filter($registrationNumbersToUpdate, function($value){
    		if ( $value ) return $value;
    	});
    	foreach ($registrationNumbersToUpdate as $regno) {
    		UpdateRegistrationNumber::dispatch($regno);
    	}
    	return 'Added to Queue';
    	
    }

    public function updateRegNumber()
    {
    	return 'here';
    }
	public function testBridge(){
		$client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);

		// $response = $client->request('GET', 'test-ks');
		// $response = (string)$response->getBody();

		$response = $client->request('POST', 'get-challan-information', [
			'json' => [ 'regno' => '2721-FLL/BSENG/F21' ]
		]);
		$challanDetails = (string)$response->getBody();

		dd($challanDetails);
	}
	public function exportTranscript($regno = null){

		// dd(base64_decode($regno));

		// $regno = '1216-FBAS/BSES/S18'; 
		$encryptedString = $regno;

		// Decrypt the string
		// $decryptedString = Crypt::decryptString($encryptedString);

		$decryptedString = base64_decode($encryptedString);


		$user = json_decode($this->getRollNoSlip($decryptedString));
		$rollnoslip = $user;

		$client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);

		$response = $client->request('get', 'get-picture-information', [
			'json' => [ 'regno' => $decryptedString ]
		]);
		$picture = (string)$response->getBody();
		
		if(empty($picture))
		  {
			  $picture = '';
		  }

		$request = $client->request('POST', 'aljamia-get-student-courses-qr', [
            'json' => [ 'regno' => $decryptedString ]
        ]);
        $semcode = json_decode((string)$request->getBody());

		return view('student.qr-detail', compact('semcode', 'picture', 'rollnoslip'));
	}

	
	public function getRollNoSlip($regno)
    {
        $rollnoslip = '';
        try {
            $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
            ]);
            $response = $client->request('get', 'get-rollnoslip-information', [
                'json' => [ 'regno' => $regno ]
            ]);
            $rollnoslip = (string)$response->getBody();
			// dd($rollnoslip);
		}catch (RequestException $e) {
            info($e);
            $rollnoslip = '';
        }
        return $rollnoslip;
    }

	public function testQr(){
		// \QrCode::size(300)->generate('https://www.example.com');

		// Data to be encoded as QR code
        $data = "Hello, World!";
        
        // Construct the Google Chart API URL
        $chartUrl = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=http://admission.iiu.edu.pk/";
        
        // Fetch the QR code image
        $qrCodeImage = file_get_contents($chartUrl);
        
        
        
        // Base64 encode the image
        $base64Image = base64_encode($qrCodeImage);

		$regno = '1216-FBAS/BSES/S18';
        
		QrCodes::updateOrCreate([
			'regno' => $regno,
			'rollnumber_slip_qr' => $base64Image,
		]);

		$result = QrCodes::where('regno', $regno)->first();
        // echo $base64Image;
        
        // Decode the base64 encoded image back to its original format
        $decodedImage = base64_decode($result->rollnumber_slip_qr);
        
        // Output the decoded image (or you can save it as needed)
        header('Content-Type: image/png');
        echo $decodedImage;


        // Output the QR code image
        // header('Content-Type: image/png');
        // echo $qrCodeImage;
	}

	public function generateQr(){
		$response = $error = '';
        if ( strtolower(request()->method()) == 'post') {
			// dd(request('regno'));

			$result = QrCodes::where('regno', request('regno'))->first();
			if(!$result){
				$encryptedString = Crypt::encryptString(request('regno'));


				$chartUrl_tr = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/transcript/".$encryptedString;
				$qrCodeImage_tr = file_get_contents($chartUrl_tr);
				$base64Image_tr = base64_encode($qrCodeImage_tr);

				$chartUrl_dg = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/degree/".$encryptedString;
				$qrCodeImage_dg = file_get_contents($chartUrl_dg);
				$base64Image_dg = base64_encode($qrCodeImage_dg);

				$chartUrl_rl = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/rollnumberslip/".$encryptedString;
				$qrCodeImage_rl = file_get_contents($chartUrl_rl);
				$base64Image_rl = base64_encode($qrCodeImage_rl);

				$chartUrl_id = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/idcard/".$encryptedString;
				$qrCodeImage_id = file_get_contents($chartUrl_id);
				$base64Image_id = base64_encode($qrCodeImage_id);

				$chartUrl_pf = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/permissionform/".$encryptedString;
				$qrCodeImage_pf = file_get_contents($chartUrl_pf);
				$base64Image_pf = base64_encode($qrCodeImage_pf);

				QrCodes::updateOrCreate([
					'regno' => request('regno')
				], [
					'transcript_qr' => $base64Image_tr,
					'degree_qr' => $base64Image_dg,
					'id_card_qr' => $base64Image_id,
					'permission_form_qr' => $base64Image_pf,
					'rollnumber_slip_qr' => $base64Image_rl
				]);

			}

			$result = QrCodes::where('regno', request('regno'))->first();
			
			$qr_degree = $result->degree_qr;
			$qr_transcript = $result->transcript_qr;



			try {
				$client = new Client([
				'base_uri' => 'http://111.68.97.170:64464/'
				]);
				$response = $client->request('POST', 'get-qr-information', [
					'json' => 
					[ 
						'regno' => request('regno'),
						'qr' => $qr_transcript
					]
				]);
				$responseDetail = (string)$response->getBody();
			} catch (RequestException $e) {
				info($e);
				$responseDetail = '';
			}
			dd($responseDetail);




			return view('student.qr-code', compact('qr_degree', 'qr_transcript'));
			// $pdf = PDF::loadView('student.qr-code', compact('qr_degree', 'qr_transcript'));
        	// return $pdf->setPaper('a4', 'portrait')->stream();
			
            // $response = $this->postAuthWithCnic(request()->all());
            // if ($response) {
            //     return $this->createUserAndAuthenticate($response);    
            // }
            // $error = 'Unable to find any user with provided details';
        }
        // return view('auth.login-with-cnic', compact('error'));
	}
}

