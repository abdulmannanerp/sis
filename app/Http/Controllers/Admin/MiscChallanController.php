<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MiscChallan;
use App\User;
use PDF;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class MiscChallanController extends Controller
{
    public function index()
    {
    	if (request()->method() == 'POST') {
    		$regno = request('regno');
            $feeCode = request('fee');
            $amount = request('amount');
            return $this->generateMiscChallan($regno, $feeCode, $amount);
    	}
    	$challans = MiscChallan::all();
    	return view('admin.misc-challan', compact('challans'));
    }

    public function generateMiscChallan($regno, $feeCode, $amount)
    {
    	$user = json_decode(
            $this->checkRegistrationNumberInAljamia($regno)
        );
    	if (! $user) {
    		return back()->with(
    			['message' => 'Invalid Registration Number', 'alert-type' => 'error']
    		);
    	}
        return $this->generateChallan($regno, $feeCode, $user, $amount);
    }

    public function checkRegistrationNubmerInLocalDatabase($regno)
    {
    	return User::where('regno', $regno)->first();
    }

    public function checkRegistrationNumberInAljamia($regno)
    {
    	$client = $this->getClient();
        $request = $client->request('POST', '/search', [
            'json' => [
                'searchBy' => 'regno',
                'query' => $regno
            ]
        ]);
        return (string)$request->getBody();
    }

    public function getClient()
    {
        return new Client([
            'base_uri' => env('BRIDGE_SERVER_URI', 'http://111.68.97.170:64464/')
        ]);
    }

    public function generateChallan($regno, $feecode, $user, $amount)
    {
        $feedetails = MiscChallan::where('feecode', $feecode)->first();
        try {
            $client = new Client([
                // 'base_uri' => 'http://usis.iiu.edu.pk:64453/fees/'
                'base_uri' => env('BRIDGE_SERVER_URI', 'http://111.68.97.170:64464/')
            ]);
            $response = $client->request('POST', 'save-misc-challan', [
                'json' => [
                    'regno' => $regno,
                    'gender' => $user->sex,
                    'miscinfo' => $feedetails,
                    'amount' => $amount 
                ]
            ]);
            $challanDetails = json_decode((string)$response->getBody());
        } catch (RequestException $e) {
            info($e);
        }
        if ($challanDetails) {
            $copies = ['Bank Copy', 'Account Copy', 'Academic Copy', 'Student Copy'];
            $pdf = PDF::loadView('student.static-challan', compact('challanDetails', 'copies', 'feedetails'));
            return $pdf->setPaper('a4', 'landscape')->stream();    
        }
        return back()->withErrors('Unable to generate challan');
    }
}
