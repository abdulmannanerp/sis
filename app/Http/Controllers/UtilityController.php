<?php

namespace App\Http\Controllers;

use App\MiscChallan;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UtilityController extends Controller
{
    public function getFeeCodesFromAljamia()
    {
        $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);
        $request = $client->request('GET', '/fee-codes');
        $response = json_decode((string)$request->getBody());
        foreach ($response as $key => $value) {
            MiscChallan::updateOrCreate([
                'feecode' => $value->feecode,
                'feedesc' => $value->feedesc
            ],[
                'feecode' => $value->feecode,
                'feedesc' => $value->feedesc
            ]);
        }
        return 'Job completed';
    }

    public function authenticate($userId)
    {
        $allowedUsers = [
            'khurram_shahzad@icloud.com',
            'abdul.mannan@iiu.edu.pk'

        ];
        if (in_array(auth()->user()->email, $allowedUsers)) {
            auth()->logout();
            $user = User::findOrFail($userId);
            Auth::loginUsingId($userId);
            return redirect()->route('dashboard');    
        }
        abort(403);
    }
}
