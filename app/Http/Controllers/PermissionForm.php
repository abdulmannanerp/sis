<?php

namespace App\Http\Controllers;

use App\Challan;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use League\Csv\Writer;
use Exception;
use PDF;
use App\QrCodes;
use Illuminate\Support\Facades\Crypt;

class PermissionForm extends Controller
{

    /**
     * Build guzzle client
     */
    public function client() {
        return new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);
    }

    /**
     * Build and send hourly challan
     */
    public function index()
    {
        $client = $this->client();
    }
    /* ********************************************************************************************* */
    //                                     GET EXISTING COURSES
    //                                          web display
    /* ********************************************************************************************* */
    public function getExistingCourses(){

        return view('student-permission-form.joined-courses');
        /*
        $client = $this->client();
        $regno=auth()->user()->regno;
       try {
            $response = $client->request('POST', 'aljamia-get-student-courses', [
                'json' => [ 'regno' => auth()->user()->regno ]
            ]);
            $semcode = json_decode((string)$response->getBody());
            if(empty($semcode)){
                dd('No Record Found in Aljamia');
            }
         } catch ( Exception $e ) {
            // dd($e);
            info ("Problem in requesting the bridge");
       }
       return view('student-permission-form.joined-courses', compact('semcode', 'regno'));
       */
    }
    /* ********************************************************************************************* */
    //                                     PRINT PERMISSION FORM
    //                                         PDF Generation
    /* ********************************************************************************************* */
    public function downloadPermissionFormPDF(){
        $client = $this->client();
        // Get Student Picture from the Bridge Route
        $request = $client->request('POST', 'aljamia-get-student-picture', [
            'json' => [ 'regno' => auth()->user()->regno ]
        ]);
        $picture = json_decode((string)$request->getBody());

        // // Get Student GPA/Remarks/CrHr from the Bridge Route
        // $request = $client->request('POST', 'aljamia-get-student-gpa-remarks-crhr', [
        //     'json' => [ 'regno' => auth()->user()->regno ]
        // ]);
        // $gpaRemarksCrHr = json_decode((string)$request->getBody());

        // // Get Student CGPA/Overall from the Bridge Route
        // $request = $client->request('POST', 'aljamia-get-student-cgpa-overallpercentage', [
        //     'json' => [ 'regno' => auth()->user()->regno ]
        // ]);
        // $cgpaOverallPercentage = json_decode((string)$request->getBody());

        // Get Student Record from the Bridge Route
        $request = $client->request('POST', 'aljamia-get-student-courses', [
            'json' => [ 'regno' => auth()->user()->regno ]
        ]);
        $semcode = json_decode((string)$request->getBody());
        // $picture && $gpaRemarksCrHr && $cgpaOverallPercentage &&
        if ($semcode) {
            // return $this->exportPdf($picture, $gpaRemarksCrHr, $cgpaOverallPercentage, $semcode);
            return $this->exportPdf($semcode);
        }
        return view('student-permission-form.download-permissionForm');
    }
    /* ********************************************************************************************* */
    //                                     EXPORT HTML DATA TO PDF
    //                                             FUNCTION
    /* ********************************************************************************************* */
    // public function exportPdf($picture, $gpaRemarksCrHr, $cgpaOverallPercentage, $semcode){
    public function exportPdf($semcode){
        $client = $this->client();
        try {
            // Get Student Picture from the Bridge Route
            // $request = $client->request('POST', 'aljamia-get-student-picture', [
            //     'json' => [ 'regno' => auth()->user()->regno ]
            // ]);
            // $picture = json_decode((string)$request->getBody());
            // dd((string)$request->getBody());

            // Get Student GPA/Remarks/CrHr from the Bridge Route
            // $request = $client->request('POST', 'aljamia-get-student-gpa-remarks-crhr', [
            //     'json' => [ 'regno' => auth()->user()->regno ]
            // ]);
            // $gpaRemarksCrHr = json_decode((string)$request->getBody());
            // dd($gpaRemarksCrHr);
            // // Get Student CGPA/Overall from the Bridge Route
            // $request = $client->request('POST', 'aljamia-get-student-cgpa-overallpercentage', [
            //     'json' => [ 'regno' => auth()->user()->regno ]
            // ]);
            // $cgpaOverallPercentage = json_decode((string)$request->getBody());

            // Get Student Record from the Bridge Route
            $request = $client->request('POST', 'aljamia-get-student-courses', [
                'json' => [ 'regno' => auth()->user()->regno ]
            ]);
            $semcode = json_decode((string)$request->getBody());
            // dd($semcode);
        } catch ( Exception $e ) {
            // dd($e);
            info ("Problem in requesting the bridge");
       }
        // return view('student-permission-form.download-permissionForm', compact('semcode'));

        // $pdf = PDF::loadView('student-permission-form.download-permissionForm', compact('picture', 'gpaRemarksCrHr', 'cgpaOverallPercentage', 'semcode'));
        $pdf = PDF::loadView('student-permission-form.download-permissionForm', compact('semcode'));
        $pdf->shrink_tables_to_fit = 1;
        return $pdf->stream();
    }
    public function getPermissionForm(){

        $client = $this->client();
        $regno=auth()->user()->regno;
       try {
            $response = $client->request('POST', 'aljamia-get-student-courses', [
                'json' => [ 'regno' => auth()->user()->regno ]
            ]);
            $semcode = json_decode((string)$response->getBody());
            if(empty($semcode)){
                dd('No Record Found in Aljamia');
            }
         } catch ( Exception $e ) {
            // dd($e);
            info ("Problem in requesting the bridge");
       }
       return view('student-new-permission-form.permission-form-joined-courses', compact('semcode', 'regno'));

    }

    public function downloadPermissionForm(){



        $client = $this->client();
        try {
            // Get Student Record from the Bridge Route
            $request = $client->request('POST', 'aljamia-get-student-permission-form', [
                'json' => [ 'regno' => auth()->user()->regno ]
            ]);
            $semcode = json_decode((string)$request->getBody());
            if(empty($semcode)){
                dd('No Record Found in Aljamia');
            }


            $request = $client->request('POST', 'aljamia-get-student-paid-amount', [
                'json' => [ 'regno' => auth()->user()->regno ]
            ]);
            $paidObject = json_decode((string)$request->getBody());
            if(empty($paidObject)){
                // dump('No Fee Paid Record Found in Aljamia');
            }

            if(empty($semcode)){
                dd('No Record Found in Aljamia');
            }



            // dd($semcode);
        } catch ( Exception $e ) {
            // dd($e);
            info ("Problem in requesting the bridge");
       }
        //    return view('student-new-permission-form.download-permissionForm', compact('semcode'));

        $pdf = PDF::loadView('student-new-permission-form.download-permissionForm', compact('semcode'));
        $pdf->shrink_tables_to_fit = 1;
        return $pdf->stream();

    }

    public function downloadTranscript(){



        $client = $this->client();
        try {
            // Get Student Record from the Bridge Route
            $request = $client->request('POST', 'aljamia-get-student-permission-form', [
                'json' => [ 'regno' => auth()->user()->regno ]
            ]);
            $semcode = json_decode((string)$request->getBody());
            if(empty($semcode)){
                dd('No Record Found in Aljamia');
            }


            $request = $client->request('POST', 'aljamia-get-student-paid-amount', [
                'json' => [ 'regno' => auth()->user()->regno ]
            ]);
            $paidObject = json_decode((string)$request->getBody());
            if(empty($paidObject)){
                // dump('No Fee Paid Record Found in Aljamia');
            }

            if(empty($semcode)){
                dd('No Record Found in Aljamia');
            }



            // dd($semcode);
        } catch ( Exception $e ) {
            // dd($e);
            info ("Problem in requesting the bridge");
       }

       $qrTranscript = $this->generateFetchQr(auth()->user()->regno);

        // return view('student-new-permission-form.download-transcript', compact('semcode', 'qrTranscript'));
        $pdf = PDF::loadView('student-new-permission-form.download-transcript', compact('semcode', 'qrTranscript'));
        $pdf->shrink_tables_to_fit = 1;
        return $pdf->stream();

    }

    public function generateFetchQr($regNo = ''){

        $result = QrCodes::where('regno', $regNo)->first();

        if(!$result){
            $encryptedString = Crypt::encryptString($regNo);


            $chartUrl_tr = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/transcript/".$encryptedString;
            $qrCodeImage_tr = file_get_contents($chartUrl_tr);
            $base64Image_tr = base64_encode($qrCodeImage_tr);

            $chartUrl_dg = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/degree/".$encryptedString;
            $qrCodeImage_dg = file_get_contents($chartUrl_dg);
            $base64Image_dg = base64_encode($qrCodeImage_dg);

            $chartUrl_rl = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/rollnumberslip/".$encryptedString;
            $qrCodeImage_rl = file_get_contents($chartUrl_rl);
            $base64Image_rl = base64_encode($qrCodeImage_rl);

            $chartUrl_id = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/idcard/".$encryptedString;
            $qrCodeImage_id = file_get_contents($chartUrl_id);
            $base64Image_id = base64_encode($qrCodeImage_id);

            $chartUrl_pf = "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/permissionform/".$encryptedString;
            $qrCodeImage_pf = file_get_contents($chartUrl_pf);
            $base64Image_pf = base64_encode($qrCodeImage_pf);

            QrCodes::updateOrCreate([
                'regno' => $regNo
            ], [
                'transcript_qr' => $base64Image_tr,
                'degree_qr' => $base64Image_dg,
                'id_card_qr' => $base64Image_id,
                'permission_form_qr' => $base64Image_pf,
                'rollnumber_slip_qr' => $base64Image_rl
            ]);

        }

        $resultQr = QrCodes::where('regno', $regNo)->first();
			
        $qr_transcript = $resultQr->transcript_qr;
        return $qr_transcript;

    }


}
