<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Exception;

class FeeVerificationController extends Controller
{
    public function index()
    {
        $challans = '';
    	if ( strtolower(request()->method()) == 'post') {
    		$challans = $this->getChallansInformationFromAljamia(
    			request('challans')
    		);
            $challans = json_decode($challans);
    	}
    	return view('fee.verify', compact('challans'));
    }

    public function getChallansInformationFromAljamia($challans)
    {
    	$challans = $this->parseChallanNumbers($challans);
    	$client = new Client([
            'base_uri' => 'http://111.68.97.170:64464'
        ]);
    	try {
    		$request = $client->request('POST', '/get-challans', [
	    		'json' => [
	    			'challans' => $challans
	    		]
	    	]);
	    	return $response = (string)$request->getBody();
    	} catch(Exception $e) {
    		info('Unable to connect to aljamia');
    		return '';
    	}
    	
    }

    public function parseChallanNumbers($challans)
    {
        $explodeBy = ',';
        if (strpos($challans, PHP_EOL) != false) {
            $explodeBy = PHP_EOL;
        }
    	$challansArray = explode($explodeBy, $challans);
    	$challansArray = array_map(function($value) {
    		if ($value) {
    			return trim($value);
    		}
    	}, $challansArray);
    	return array_unique($challansArray);
    }

    public function verifyChallans()
    {
        $challanNumbers = explode(',', request('challanNumbers'));
        if ($challanNumbers) {
            try {
                $client = new Client([
                    'base_uri' => '111.68.97.170:64464'
                ]);
                $request = $client->request('POST', '/verify-challans', [
                    'json' => [
                        'challans' => $challanNumbers,
                        'semCode' => 'FALL-2020',
                        'aljamiaUserLogin' => auth()->user()->aljamia_id ?? auth()->id()
                    ]
                ]);
                $response = (string)$request->getBody();
                return redirect()
                    ->route('feeVerify')
                    ->with(['message' => $response, 'alert-type' => 'success']);
            } catch (Exception $e) {
                $response = 'Unable to connect to aljamia, Please try again later';
                return redirect()
                    ->route('feeVerify')
                    ->with(['message' => $response, 'alert-type' => 'error']);
            } 
        }
    }
}
