<?php

namespace App\Http\Controllers;

use App\Challan;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use League\Csv\Writer;
use Exception;

class ChallanController extends Controller
{

    /**
     * Build guzzle client
     */
    public function client() {
        return new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);
    }

    /**
     * Build and send hourly challan
     */
    public function index()
    {
        $client = $this->client();
        try {
            // Send challan to abl
            $request = $client->request('GET', '/bulid-challan-csv-abl');
            $response = json_decode((string)$request->getBody());
            // if (!$response) {
            //     return 'No challans to send ABL';
            // }
            if($response){
                $csvPath = $this->prepareCsv($response);
                try {
                    $this->sendEmail($csvPath);
                } catch (Exception $e) {
                    $exception = 'Email not sent To ABL';
                    info($e);
                    info($exception);
                    return $exception;
                    //send email to khurram about mail exception at time
                }
            }
            // send challan for FWBL
            $requestFwbl = $client->request('GET', '/bulid-challan-csv-abl-fwbl');
            $responseFwbl = json_decode((string)$requestFwbl->getBody());
            if($responseFwbl){
                $csvPathFwbl = $this->prepareCsv($responseFwbl);
                try {
                    $this->sendEmailFwbl($csvPathFwbl);
                } catch (Exception $e) {
                    $exception = 'Email not sent To FWBL';
                    info($e);
                    info($exception);
                    return $exception;
                    //send email to khurram about mail exception at time
                }
            }
            /* ***************************************************************** */
            /*                   SEND CHALLAN TO FAYSAL BANK Start               */
            /* ***************************************************************** */
            $requestFaysal = $client->request('GET', '/bulid-challan-csv-faysal');
            $responseFaysal = json_decode((string)$requestFaysal->getBody());
            if($responseFaysal){
                $csvPathFaysal = $this->prepareCsvFaysalBank($responseFaysal);
                try {
                    $this->sendEmailFaysal($csvPathFaysal);
                } catch (Exception $e) {
                    $exception = 'Email not sent To FWBL';
                    info($e);
                    info($exception);
                    return $exception;
                    //send email to khurram about mail exception at time
                }
            }
            /* ***************************************************************** */
            /*                                      END                          */
            /* ***************************************************************** */


             /* ***************************************************************** */
            /*                  Dev Mannan: SEND CHALLAN TO Bank Alfalah Start    */
            /* ***************************************************************** */
            $requestAlfalah = $client->request('GET', '/bulid-challan-csv-alfalah');
            $requestAlfalah = json_decode((string)$requestAlfalah->getBody());
            if($requestAlfalah){
                $csvPathAlfalah = $this->prepareCsvAlfalahlBank($requestAlfalah);
                try {
                    $this->sendEmailAlfalah($csvPathAlfalah);
                } catch (Exception $e) {
                    $exception = 'Email not sent To BAFL';
                    info($e);
                    info($exception);
                    return $exception;
                    //send email to khurram about mail exception at time
                }
            }
            /* ***************************************************************** */
            /*                         Dev Mannan:   END                          */
            /* ***************************************************************** */


            return 'Email Sent';
        } catch (RequestException $e) {
            $exception = 'Exception getting records form aljamia';
            info($e);
            info($exception);
            return $exception;
            //send email to khurram about exception
        }
    }

    /**
     * Trigger the email to ABL
     */
    public function sendEmail($filePath)
    {
        $email_host = 'smtp-relay.gmail.com';
        $email_port = '587';
        $email_encryption = 'tls';
        $email_username = 'fee.challans@iiu.edu.pk';
        // $email_password = 'imcpcbrvknmmarzw'; // Wrong Password
        // $email_password = 'jdBTw$v@sT1^'; //Current
        $email_password = 'kqagwvocxrcaxxeq'; //App Password


        Config::set('mail.driver', 'smtp');
        Config::set('mail.host', $email_host);
        Config::set('mail.port', $email_port);
        Config::set('mail.encryption', $email_encryption);
        Config::set('mail.username', $email_username);
        Config::set('mail.password', $email_password);

        $to ='T24SysAdmins@abl.com';
        // $to ='abdul.mannan@iiu.edu.pk';
        // $to ='shakir.rasheed@iiu.edu.pk';
        $subject = 'IIU Fee Challans (ABL)';
        Mail::send([], [], function ($message) use ($to, $subject, $email_username, $filePath) {
            $message->to($to)
                ->cc(['zafar.kamran@abl.com','mudassir.ahmed@iiu.edu.pk', 'mariam.javed@iiu.edu.pk', 'sumreena.jabeen@iiu.edu.pk' , 'fee.challans@iiu.edu.pk', 'abdulhameed@iiu.edu.pk'])
                ->subject($subject);
            $message->from($email_username, 'IIU Challans Data');
            $message->attach($filePath);
        });
    }
    /**
     * Trigger the email to FWBL
     */
    public function sendEmailFwbl($filePath)
    {
        $email_host = 'smtp-relay.gmail.com';
        $email_port = '587';
        $email_encryption = 'tls';
        $email_username = 'fee.challans@iiu.edu.pk';
        // $email_password = 'rkrkudjzuhybzqay';
        $email_password = 'kqagwvocxrcaxxeq'; // APP PASSWORD

        Config::set('mail.driver', 'smtp');
        Config::set('mail.host', $email_host);
        Config::set('mail.port', $email_port);
        Config::set('mail.encryption', $email_encryption);
        Config::set('mail.username', $email_username);
        Config::set('mail.password', $email_password);

        // $to ='afsheen.akhtar@fwbl.com.pk';
        $to ='shakir.rasheed@iiu.edu.pk';
        $subject = 'IIU Fee Challans FWBL';
        Mail::send([], [], function ($message) use ($to, $subject, $email_username, $filePath) {
            $message->to($to)
                // ->cc(['nadia.hafeez@iiu.edu.pk', 'nishwa.iqbal@iiu.edu.pk', 'mariam.javed@iiu.edu.pk', 'fee.challans@iiu.edu.pk', 'Shazia.Razaq@abl.com'])
                ->subject($subject);
            $message->from($email_username, 'IIU Challans Data');
            $message->attach($filePath);
        });
    }
    /* ********************************************************************* */ 
    //                  TRIGER EMAIL TO FAYSAL BANK START
    /* ********************************************************************* */
    public function sendEmailFaysal($filePath)
    {
        $email_host = 'smtp-relay.gmail.com';
        $email_port = '587';
        $email_encryption = 'tls';
        $email_username = 'fee.challans@iiu.edu.pk';
        // $email_password = 'jdBTw$v@sT1^'; //Current
        $email_password = 'kqagwvocxrcaxxeq'; // APP PASSWORD

        Config::set('mail.driver', 'smtp');
        Config::set('mail.host', $email_host);
        Config::set('mail.port', $email_port);
        Config::set('mail.encryption', $email_encryption);
        Config::set('mail.username', $email_username);
        Config::set('mail.password', $email_password);

        // $to ='haroonjaswal@faysalbank.com';
        $to ='faysaltransact@faysalbank.com';
        // $to ='shakir.rasheed@iiu.edu.pk';
        $subject = 'IIU Fee Challans (Faysal Bank)';
        Mail::send([], [], function ($message) use ($to, $subject, $email_username, $filePath) {
            $message->to($to)
                ->cc(['khalid.hussain@iiu.edu.pk', 'arifsaleem@iiu.edu.pk', 'fee.challans@iiu.edu.pk', 'abdulhameed@iiu.edu.pk','mudassir.ahmed@iiu.edu.pk', 'mubasharahmed@faysalbank.com', 'shahzadaFahad@faysalbank.com', 'munawarMazhar@faysalbank.com', 'haroonJaswal@faysalbank.com' ])
                ->subject($subject);
            $message->from($email_username, 'IIU Challans Data');
            $message->attach($filePath);
        });
    }

    public function sendEmailAlfalah($filePath)
    {
        $email_host = 'smtp-relay.gmail.com';
        $email_port = '587';
        $email_encryption = 'tls';
        $email_username = 'fee.challans@iiu.edu.pk';
        // $email_password = 'jdBTw$v@sT1^'; //Current
        $email_password = 'kqagwvocxrcaxxeq'; // APP PASSWORD

        Config::set('mail.driver', 'smtp');
        Config::set('mail.host', $email_host);
        Config::set('mail.port', $email_port);
        Config::set('mail.encryption', $email_encryption);
        Config::set('mail.username', $email_username);
        Config::set('mail.password', $email_password);

        // $to ='abdul.dev.mannan@gmail.com';
        $to ='saqib.al@bankalfalah.com';
        $subject = 'IIU Fee Challans (Alfalah Bank)';
        Mail::send([], [], function ($message) use ($to, $subject, $email_username, $filePath) {
            $message->to($to)
                ->cc(['khalid.hussain@iiu.edu.pk', 'arifsaleem@iiu.edu.pk', 'fee.challans@iiu.edu.pk', 'abdulhameed@iiu.edu.pk','mudassir.ahmed@iiu.edu.pk', 'saqib.al@bankalfalah.com' ])
                // ->cc(['usman.ali@iiu.edu.pk'])
                ->subject($subject);
            $message->from($email_username, 'IIU Challans Data');
            $message->attach($filePath);
        });
    }
    // cc(['arifsaleem@iiu.edu.pk', 'fee.challans@iiu.edu.pk', 'abdulhameed@iiu.edu.pk'])
    /* ********************************************************************* */
    //                                    END 
    /* ********************************************************************* */

    /**
     * Prepare the CSV which will be attached in email
     */
    public function prepareCsv($response, $insertRecordsToDatabase = true)
    {
        $dateTimeNow = date('d-M-Y-H-i-s');
        $path = storage_path('app/allied-bank-challans/'.$dateTimeNow.'-ABL.csv');
        $csv = Writer::createFromPath($path, 'w');
        $csv->insertOne([
            'Issue_date',
            'Challan_No',
            'ClassName',
            'Bank_Account',
            'Amount',
            'Narration',
            'Profit_Center',
            'Std_Id',
            'SP_GI',
            'Std_Name',
            'Ins_No',
            'Year',
            'Due_date',
            'Valid_date',
            'Status'
        ]);
        $records = [];
        foreach ($response as $key => $value) {
            $record = [];
            $record['Issue_date'] = date_format(date_create($value->challandate), 'Ymd') ?? ''; //issue_date
            $record['Challan_No'] = $value->challanno ?? ''; //challan_no
            $record['ClassName'] = $value->acadprogname ?? ''; //class_name
            $record['Bank_Account'] = ($value->bankaccno) ? str_replace('-', '', $value->bankaccno) : ''; //bank_account
            $record['Amount'] = $value->challanamnt ?? ''; //amount
            $record['Narration'] = $value->facname ?? ''; //narration
            $record['Profit_Center'] = ''; //profilt_center
            $record['Std_Id'] = $value->regno ?? ''; //std_id
            $record['SP_GI'] = ''; //sp_gi
            $record['Std_Name'] = $value->studname ?? ''; //std_name
            $record['Ins_No'] = ''; //ins_no
            $record['Year'] = $value->semcode; //year
            $record['Due_date'] = date_format(date_create($value->challanduedate), 'Ymd'); //due_date
            $record['Valid_date'] = date('Ymd', strtotime("+1 day", strtotime($value->challanduedate))); //valid_date
            $record['Status'] = $value->challanstatus;
            $records[] = $record;
        }
        $csv->insertAll($records);
        if ($insertRecordsToDatabase) {
            $recordsWithFilePath = [];
            foreach ($records as $record) {
                $record['file_path'] = $path;
                $recordsWithFilePath[] = $record;
            }

            Challan::insert($recordsWithFilePath);
        }

        return $path;
    }

    public function prepareCsvHbl($response, $insertRecordsToDatabase = true)
    {
        $dateTimeNow = date('d-M-Y-H-i-s');
        $path = storage_path('app/allied-bank-challans/'.$dateTimeNow.'-HBL.csv');
        $csv = Writer::createFromPath($path, 'w');
        $csv->insertOne([
            'Challan_No',
            'Registration_Number',
            'Roll_No',
            'Student_Name',
            'CNIC',
            'Faculty',
            'Department'
        ]);
        $records = [];
        foreach ($response as $key => $value) {
            $record = [];
            $record['Challan_No'] = $value->challanno ?? ''; //challan_no
            $record['Registration_Number'] = $value->regno ?? ''; //std_id
            $record['Roll_No'] = '';
            $record['Student_Name'] = $value->studname ?? ''; //std_name
            $record['CNIC'] = '';
            $record['Faculty'] = $value->facname ?? ''; //narration
            $record['Department'] = 'Department Of '.$value->depname ?? '';
            $records[] = $record;
        }
        $csv->insertAll($records);
        if ($insertRecordsToDatabase) {
            $recordsWithFilePath = [];
            foreach ($records as $record) {
                $record['file_path'] = $path;
                $recordsWithFilePath[] = $record;
            }

            Challan::insert($recordsWithFilePath);
        }

        return $path;
    }

    /* ********************************************************************* */
    //                         SEND CHALLAN TO FAYSAL BANK START
    /* ********************************************************************* */
    public function prepareCsvFaysalBank($response, $insertRecordsToDatabase = true)
    {
        $dateTimeNow = date('d-M-Y-H-i-s');
        $path = storage_path('app/faysalbank-challans/'.$dateTimeNow.'-FBL.csv');
        $csv = Writer::createFromPath($path, 'w');
        $csv->insertOne([
            'Issue_date',
            'Challan_No',
            'ClassName',
            'Bank_Account',
            'Amount',
            'Narration',
            'Profit_Center',
            'Std_Id',
            'SP_GI',
            'Std_Name',
            'Ins_No',
            'Year',
            'Due_date',
            'Valid_date',
            'Status'
        ]);
        $records = [];
        foreach ($response as $key => $value) {
            $record = [];
            $record['Issue_date'] = date_format(date_create($value->challandate), 'Ymd') ?? ''; //issue_date
            $record['Challan_No'] = $value->challanno ?? ''; //challan_no
            $record['ClassName'] = $value->acadprogname ?? ''; //class_name
            $record['Bank_Account'] = ($value->bankaccno2) ? str_replace('-', '', $value->bankaccno2) : ''; //bank_account
            $record['Amount'] = $value->challanamnt ?? ''; //amount
            $record['Narration'] = $value->facname ?? ''; //narration
            $record['Profit_Center'] = ''; //profilt_center
            $record['Std_Id'] = $value->regno ?? ''; //std_id
            $record['SP_GI'] = ''; //sp_gi
            $record['Std_Name'] = $value->studname ?? ''; //std_name
            $record['Ins_No'] = ''; //ins_no
            $record['Year'] = $value->semcode; //year
            $record['Due_date'] = date_format(date_create($value->challanduedate), 'Ymd'); //due_date
            $record['Valid_date'] = date('Ymd', strtotime("+1 day", strtotime($value->challanduedate))); //valid_date
            $record['Status'] = $value->challanstatus;
            $records[] = $record;
        }
        $csv->insertAll($records);
        if ($insertRecordsToDatabase) {
            $recordsWithFilePath = [];
            foreach ($records as $record) {
                $record['file_path'] = $path;
                $recordsWithFilePath[] = $record;
            }

            Challan::insert($recordsWithFilePath);
        }

        return $path;
    }
    /* ********************************************************************* */
    //                                    END          
    /* ********************************************************************* */

    /**
     * Admin menu, to manually resend the challan
     */

     public function prepareCsvAlfalahlBank($response, $insertRecordsToDatabase = true)
     {
         $dateTimeNow = date('d-M-Y-H-i-s');
         $path = storage_path('app/alfalahbank-challans/'.$dateTimeNow.'-AFL.csv');
         $csv = Writer::createFromPath($path, 'w');
         $csv->insertOne([
             'Issue_date',
             'Challan_No',
             'ClassName',
             'Bank_Account',
             'Amount',
             'Narration',
             'Profit_Center',
             'Std_Id',
             'SP_GI',
             'Std_Name',
             'Ins_No',
             'Year',
             'Due_date',
             'Valid_date',
             'Status'
         ]);
         $records = [];
         foreach ($response as $key => $value) {
             $record = [];
             $record['Issue_date'] = date_format(date_create($value->challandate), 'Ymd') ?? ''; //issue_date
             $record['Challan_No'] = $value->challanno ?? ''; //challan_no
             $record['ClassName'] = $value->acadprogname ?? ''; //class_name
             $record['Bank_Account'] = ($value->bankaccno2) ? str_replace('-', '', $value->bankaccno2) : ''; //bank_account
             $record['Amount'] = $value->challanamnt ?? ''; //amount
             $record['Narration'] = $value->facname ?? ''; //narration
             $record['Profit_Center'] = ''; //profilt_center
             $record['Std_Id'] = $value->regno ?? ''; //std_id
             $record['SP_GI'] = ''; //sp_gi
             $record['Std_Name'] = $value->studname ?? ''; //std_name
             $record['Ins_No'] = ''; //ins_no
             $record['Year'] = $value->semcode; //year
             $record['Due_date'] = date_format(date_create($value->challanduedate), 'Ymd'); //due_date
             $record['Valid_date'] = date('Ymd', strtotime("+1 day", strtotime($value->challanduedate))); //valid_date
             $record['Status'] = $value->challanstatus;
             $records[] = $record;
         }
         $csv->insertAll($records);
         if ($insertRecordsToDatabase) {
             $recordsWithFilePath = [];
             foreach ($records as $record) {
                 $record['file_path'] = $path;
                 $recordsWithFilePath[] = $record;
             }
 
             Challan::insert($recordsWithFilePath);
         }
 
         return $path;
     }
    public function resendChallan()
    {
        // die("resendChallan Method in ChallanController.php SRKK");
        $csvPath = '';
        if (strtolower(request()->method()) == 'post') {
            $selectedDate = date_format(date_create(request('date')), 'd-M-Y');
            $csvPath = $this->buildSelectedDateChallan($selectedDate, request('bankName'));
        }
        return view('challan.resend-challan', [
            'csvPath' => $csvPath,
            'bank' => request('bankName'),
        ]);
    }

    /**
     * CRON that sends daily challan
     */
    public function sendCurrentDateChallan()
    {
        die(request('bank'));

        $currentDate = date('d-M-Y');
        $path = $this->buildSelectedDateChallan($currentDate);
        if(! $path) {
            info('Unable to send daily email '. $currentDate);
        }else if(request('bank') == 'fbl'){
            $this->sendEmailFaysal($path);
        }else{
            $this->sendEmail($path);
        }
        
        return 'Done';
    }

    /**
     * Admin menu which builds selected day challan file
     */
    public function buildSelectedDateChallan($selectedDate, $bankName)
    {
        $client = $this->client();
        try {
            $request = $client->request('POST', '/get-challans-by-date', [
                'json' => [
                    'date' => $selectedDate,
                    'bankName' => $bankName,
                ]
            ]);
            $response = json_decode((string)$request->getBody());
            // print_r($response);die();
            //echo $bankName; exit;
            if (!$response) {
                return;
            }
            // if($bankName == 'hbl'){
            //     // return $csvPath = $this->prepareCsvHbl($response, false);
            //     return $csvPath = $this->prepareCsv($response, false);
            // }else if($bankName == 'faysal'){
            //     return $csvPath = $this->prepareCsvFaysalBank($response, false);
            // }
            if($bankName == 'alfalah'){
                return $csvPath = $this->prepareCsvAlfalahlBank($response, false);
            }
            else{
                //die($bankName);
                if($bankName == 'fbl')
                    return $csvPath = $this->prepareCsvFaysalBank($response, false);
                else
                    return $csvPath = $this->prepareCsv($response, false);
             }

        } catch (RequestException $e) {
            return;
        }
    }

    /**
     * Admin, Review the build challan for selected date
     */
    public function downloadCsvFile()
    {
        return response()->download(
            request('path')
        );
    }

    public function emailSingleDayChallansToBank()
    {
        // die(request('bank'));
        // die(request('path'));
        if(request('bank') == 'abl'){
            $this->sendEmail(request('path'));
        }
        if(request('bank') == 'fbl'){
            $this->sendEmailFaysal(request('path'));
            // die(request('path'));
        }
        if(request('bank') == 'fwbl'){
            $this->sendEmailFwbl(request('path'));
        }
        if(request('bank') == 'alfalah'){
            $this->sendEmailAlfalah(request('path'));
        }
        return back();
    }

    public function modifyChallan()
    {
        $challanDetails = $challanNumber = '';

        if (request()->method() == 'POST') {
            $challanNumber = request('challanNumber');
            $challan = $this->searchChallan($challanNumber);
            if ($challan) {
                $challanDetails = json_decode($challan);
            }

        }
        return view('challan.modify', [
            'challanDetails' => $challanDetails,
            'challanNumber' => $challanNumber
        ]);
    }

    /**
     * Admin search by challan number
     */
    public function searchChallan($query)
    {
        $client = new Client([
            'base_uri' => env('BRIDGE_SERVER_URI')
        ]);
        $request = $client->request('POST', '/search', [
            'json' => [
                'searchBy' => 'challan',
                'query' => $query
            ]
        ]);
        return (string)$request->getBody();
    }
}
