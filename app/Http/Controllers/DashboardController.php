<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Land user to here after authentication
     */
    public function index()
    {
        return view('student.dashboard');
    }
}
