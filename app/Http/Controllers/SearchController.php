<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {
        $response = '';
        if (strtolower(request()->method()) == 'post') {
            $response = $this->getInformationFromAljamia(request()->all());
            if ($response) {
                $response = json_decode($response);
            }
        }
        
        return view('search.index', compact('response'));
    }

    public function getInformationFromAljamia($request)
    {
        $query = $request['query'];
        $searchBy = $request['searchBy'];

        switch ($searchBy) {
            case 'regno':
                return $this->getResultsByRegistrationNumber($query);
                break;
            case 'challan':
                return $this->getResultsByChallanNumber($query);
                break;
            case 'email':
                return $this->getResultsByEmailAddress($query);
                break;
        }
    }

    public function getResultsByRegistrationNumber($query)
    {
        $client = $this->getClient();
        $request = $client->request('POST', '/search', [
            'json' => [
                'searchBy' => 'regno',
                'query' => $query
            ]
        ]);
        return (string)$request->getBody();
    }

    public function getResultsByChallanNumber($query)
    {
        $client = $this->getClient();
        $request = $client->request('POST', '/search', [
            'json' => [
                'searchBy' => 'challan',
                'query' => $query
            ]
        ]);
        return (string)$request->getBody();
    }

    public function getResultsByEmailAddress($query)
    {
        $client = $this->getClient();
        $request = $client->request('POST', '/search', [
            'json' => [
                'searchBy' => 'email',
                'query' => $query
            ]
        ]);
        return (string)$request->getBody();
    }

    public function getClient()
    {
        return new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);
    }
}
