<?php

namespace App\Http\Controllers;

use App\Challan;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use League\Csv\Writer;
use Exception;
use PDF;

class ResultIntimation extends Controller
{

    /**
     * Build guzzle client
     */
    public function client() {
        return new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);
    }

    /**
     * Build and send hourly challan
     */
    public function index()
    {
        $client = $this->client();
    }
    /* ********************************************************************************************* */
    //                                     GET EXISTING COURSES
    //                                          web display
    /* ********************************************************************************************* */
    public function getExistingCourses(){
        $client = $this->client();
        $regno=auth()->user()->regno;
       try {
            $response = $client->request('POST', 'aljamia-get-student-courses', [
                'json' => [ 'regno' => auth()->user()->regno ]
            ]);
            $semcode = json_decode((string)$response->getBody());
         } catch ( Exception $e ) {
            // dd($e);
            info ("Problem in requesting the bridge");
       }
       return view('semester-result-intimation.joined-courses', compact('semcode', 'regno'));
    }
    /* ********************************************************************************************* */
    //                                     PRINT PERMISSION FORM
    //                                         PDF Generation
    /* ********************************************************************************************* */
    public function downloadResultIntimationPDF(){

        if (strtolower(request()->method()) == 'post') {

            $semester = request('semester').'-'.request('year');
            if($semester == 'FALL-2024') {
                dd('FALL-2024 result has not been declared yet');
            }

        }

        $client = $this->client();

        // Get Student Record from the Bridge Route
        $request = $client->request('POST', 'aljamia-get-student-intimation', [
            'json' => [ 'regno' => auth()->user()->regno, 'semester' =>  $semester ]
        ]);
        $semcode = json_decode((string)$request->getBody());
        if(empty($semcode)){
            dd('No Record Found in Aljamia');
        }
        // $picture && $gpaRemarksCrHr && $cgpaOverallPercentage &&
        if ($semcode) {
            // return $this->exportPdf($picture, $gpaRemarksCrHr, $cgpaOverallPercentage, $semcode);
            return $this->exportPdf($semcode, $semester);
        }

        return view('semester-result-intimation.joined-courses', compact('semester'));
    }
    /* ********************************************************************************************* */
    //                                     EXPORT HTML DATA TO PDF
    //                                             FUNCTION
    /* ********************************************************************************************* */
    // public function exportPdf($picture, $gpaRemarksCrHr, $cgpaOverallPercentage, $semcode){
    public function exportPdf($semcode, $semester = ''){
        $client = $this->client();
    //     try {
    //         // Get Student Record from the Bridge Route
    //         $request = $client->request('POST', 'aljamia-get-student-courses', [
    //             'json' => [ 'regno' => auth()->user()->regno ]
    //         ]);
    //         $semcode = json_decode((string)$request->getBody());
    //         // dd($semcode);
    //     } catch ( Exception $e ) {
    //         // dd($e);
    //         info ("Problem in requesting the bridge");
    //    }
       $regno = auth()->user()->regno;
       $response = $client->request('get', 'get-picture-information', [
                'json' => [ 'regno' => $regno ]
            ]);
            $picture = (string)$response->getBody();
        // return view('student-permission-form.download-permissionForm', compact('semcode'));

        // $pdf = PDF::loadView('student-permission-form.download-permissionForm', compact('picture', 'gpaRemarksCrHr', 'cgpaOverallPercentage', 'semcode'));
        $pdf = PDF::loadView('student-permission-form.result-intimation', compact('semcode','regno', 'picture', 'semester'));
        $pdf->shrink_tables_to_fit = 1;
        return $pdf->stream();
    }
}
