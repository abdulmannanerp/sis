<?php

namespace App\Http\Controllers;

use App\Models\Batch;
use App\Models\Program;
use App\Models\Semester;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class AljamiaResyncController extends Controller
{
    public function index($model)
    {
    	switch ($model) {
    		case 'faculty':
    			return $this->syncFaculties();
    			break;
    		
    		case 'department':
    			return $this->syncDepartments();
    			break;

    		case 'program':
    			return $this->syncPrograms();
    			break;

    		case 'batch':
    			return $this->syncBatches();
    			break;

    		case 'semester':
    			return $this->syncSemesters();
    			break;
    	}
    }

    public function syncFaculties()
    {
    	return 'here';
    }

    public function syncDepartments()
    {
    	return 'here';
    }

    public function syncPrograms()
    {
    	$route = '/program';
        $programs = $this->resync($route);
        foreach ($programs as $program) {
            Program::updateOrCreate([
                'acadprogcode' => $program->acadprogcode
            ], [
                'acadprogcode' => $program->acadprogcode,
                'acadprogname' => $program->acadprogname,
                'maxcredithrs' => $program->maxcredithrs,
                'acadprogdesc' => $program->acadprogdesc,
                'depcode' => $program->depcode,
                'apalias' => $program->apalias,
                'faccode' => $program->faccode,
                'degreeduration' => $program->degreeduration,
                'acadproglevel' => $program->acadproglevel
            ]);
        }
        return 'done';
    }

    public function syncBatches()
    {
        $route = '/batch';
    	$batches = $this->resync($route);
        foreach ($batches as $batch) {
            Batch::updateOrCreate([
                'batchcode' => $batch->batchcode
            ], [
                'batchname' => $batch->batchname,
                'maxcredithrs' => $batch->maxcredithrs,
                'batchtime' => $batch->batchtime,
                'acadprogcode' => $batch->acadprogcode,
                'regnotemplate' => $batch->regnotemplate,
                'passmarks' => $batch->passmarks,
                'probrangestart' => $batch->probrangestart,
                'probrangeend' => $batch->probrangeend,
                'noofprobs' => $batch->noofprobs,
                'sex' => $batch->sex,
                'yearofadm' => $batch->yearofadm,
                'depcode' => $batch->depcode,
                'faccode' => $batch->faccode,
                'specializationoffered' => $batch->specializationoffered,
                'majorminor' => $batch->majorminor,
                'noofcore' => $batch->noofcore,
                'noofmajor' => $batch->noofmajor,
                'noofminor' => $batch->noofminor,
                'thesisproject' =>$batch->thesisproject,
                'noofthesisoptional' => $batch->noofthesisoptional,
                'sofnamecode' => $batch->sofnamecode,
                'urfee' => $batch->urfee
            ]);
        }
        return 'done';
    }

    public function syncSemesters()
    {
        $route = '/semester';
        $semesters = $this->resync($route);
        foreach ($semesters as $semester) {
            Semester::updateOrCreate([
                'semcode' => $semester->semcode
            ], [
                'semstartdate' => $semester->semstartdate,
                'semenddate' => $semester->semenddate,
                'season' => $semester->season,
                'joindeadline' => $semester->joindeadline,
                'probcountstatus' => $semester->probcountstatus
            ]);
        }
        return 'here';
    }

    public function resync($route){
        $client = new Client([
            'base_uri' => env('BRIDGE_SERVER_URI')
        ]);
        $request = $client->request('get', $route);
        return json_decode( (string) $request->getBody() );
    }
}
