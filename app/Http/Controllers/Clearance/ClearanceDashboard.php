<?php

namespace App\Http\Controllers\Clearance;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class ClearanceDashboard extends Controller
{
    /**
     * Land user to here after authentication
     */
    public function index()
    {
		 $user = auth()->user();
		 if(!$user){
			 return redirect()->route('login');
		 }
		 //$userdata=$this->getUserDetailsFromAljamia($user->email);
		 $userdata=$this->getUserDetailsFromAljamia('abdul.mscs831@iiu.edu.pk');
		 dd($userdata);
		$degreeCompleted=0;
		if($userdata->status=='DC'){
			$degreeCompleted=1;
		}
		$hifzTestPassed=0;
		if($userdata->hifz=='Y'){
			$hifzTestPassed=1;
		}
        return view('clearance.dashboard',[
		'degreeCompleted'=>$degreeCompleted,
		'hifzTestPassed'=>$hifzTestPassed
		]);
    }
	public function getUserDetailsFromAljamia($email)
    {
        $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);
        $request = $client->request('POST', 'get-student-by-email', [
            'json' => ['email' => $email]  
        ]);
        return json_decode((string)$request->getBody());
    }
}
