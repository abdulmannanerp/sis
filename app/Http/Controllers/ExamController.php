<?php

namespace App\Http\Controllers;

use PDF;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use App\Feecodes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use DB;
use App\QrCodes;

class ExamController extends Controller
{
    public function index()
    {		
		// dd('Roll Number Slip Will be Open Soon. Please wait');
		$user = auth()->user();
		$regno = $user->regno;
		
		$defaultstud  = DB::table('defaulter_fees_stud')
                         ->where('regno', $regno)
						 ->where('status', 1)
                         ->first();
		/*if(!empty($defaultstud) && $defaultstud->regno == $regno && $defaultstud->status == 1)
		   {
			   //return redirect('dashboard')->withErrors(['more information Contact to Fee Section.', 'The Message']);
		   }
		else{*/
				if($this->isStudentDefaulter($user->regno)){
					return redirect('/dashboard')->withErrors(['Please Contact FEE Section Your Status is Defaulter once Cleared from Fee, Next You have to Visit AUTOMATION Section', 'The Message']);
				}
				if($this->isAdmissionCancelled($user->regno)){
					return redirect('/dashboard')->withErrors(['Your Admission Status Is Cancelled', 'The Message']);
				}
				
				$user = json_decode($this->getRollNoSlip($user->regno));
				if(empty($user[0]->faccode))
				 {
				  return redirect('/dashboard')->withErrors(['Course joining is missing in Aljamia SIS. Please contact Program/Department Coordinator.', 'The Message']);
				 }
				$faccode = $user[0]->faccode;
				$acadprogcode = $user[0]->acadprogcode;
				$batchcode = $user[0]->batchcode;
				$semcode = $user[0]->semcode;
				
				$client = new Client([
					'base_uri' => 'http://111.68.97.170:64464/'
					]);
					$response = $client->request('get', 'get-picture-information', [
						'json' => [ 'regno' => $regno ]
					]);
					$picture = (string)$response->getBody();
					
					if(empty($picture))
					  {
						  $picture = '';
					  }
					
					$responsefac = $client->request('get', 'get-faculty-information', [
						'json' => [ 'faccode' => $faccode ]
					]);
					$faculty = json_decode((string)$responsefac->getBody());
					
					$responseacad = $client->request('get', 'get-acadprograme-information', [
						'json' => [ 'acadprogcode' => $acadprogcode ]
					]);
					$acadprog = json_decode((string)$responseacad->getBody());
					
					$responsebatch = $client->request('get', 'get-batch-information', [
						'json' => [ 'batchcode' => $batchcode ]
					]);
				
					$batch = json_decode((string)$responsebatch->getBody());
					
					$responsesemdate = $client->request('get', 'get-semdate-information', [
						'json' => [ 'semcode' => $semcode ]
					]);
				
					$semdate = json_decode((string)$responsesemdate->getBody());
				
				    $rollnoslip = $user;

					// Dev Mannan: Qr Code starts

					// $regno = '1216-FBAS/BSES/S18';
					// $result = QrCodes::where('regno', $regno)->first();
					// $decodedImage = $result->rollnumber_slip_qr;

					// Dev Mannan: Qr Code Ends
				
				return view('exam.rollno-slip', compact('user', 'rollnoslip', 'picture', 'faculty', 'acadprog', 'batch', 'defaultstud', 'semdate'));        
    }
	public function isStudentDefaulter($regno){
		$isdefaulter = 1;
        try {
            $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
            ]);
            $response = $client->request('get', 'is-student-defaulter', [
                'json' => [ 'regno' => $regno ]
            ]);
            $isdefaulter = (string)$response->getBody();
			// dd($rollnoslip);
		}catch (RequestException $e) {
            info($e);
        }
        return $isdefaulter;
	}
	/*
	public function isStudentPrevented($regno){
		$isPrevented = 1;
        try {
            $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
            ]);
            $response = $client->request('get', 'is-student-prevented', [
                'json' => [ 'regno' => $regno ]
            ]);
            $isPrevented = (string)$response->getBody();
			// dd($rollnoslip);
		}catch (RequestException $e) {
            info($e);
        }
        return $isPrevented;
	}
		*/
	public function isAdmissionCancelled($regno){
		$isdefaulter = 1;
        try {
            $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
            ]);
            $response = $client->request('get', 'is-admission-cancelled', [
                'json' => [ 'regno' => $regno ]
            ]);
            $isdefaulter = (string)$response->getBody();
			// dd($rollnoslip);
		}catch (RequestException $e) {
            info($e);
        }
        return $isdefaulter;
	}
    /**
     * Request aljmaia for Roll No Slip
     */
    public function getRollNoSlip($regno)
    {
        $rollnoslip = '';
        try {
            $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
            ]);
            $response = $client->request('get', 'get-rollnoslip-information', [
                'json' => [ 'regno' => $regno ]
            ]);
            $rollnoslip = (string)$response->getBody();
			// dd($rollnoslip);
		}catch (RequestException $e) {
            info($e);
            $rollnoslip = '';
        }
        return $rollnoslip;
    }
	
    /**
    * Build pdf for a Roll No Slip
    */
    public function exportPdf($id = null)
    {
        $user = auth()->user();
		$regno = $user->regno;
		$user = json_decode($this->getRollNoSlip($user->regno));
		$faccode = $user[0]->faccode;
		$acadprogcode = $user[0]->acadprogcode;
		$batchcode = $user[0]->batchcode;
		$semcode = $user[0]->semcode;
		
		$client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
            ]);
            $response = $client->request('get', 'get-picture-information', [
                'json' => [ 'regno' => $regno ]
            ]);
            $picture = (string)$response->getBody();
			
			$responsefac = $client->request('get', 'get-faculty-information', [
                'json' => [ 'faccode' => $faccode ]
            ]);
            $faculty = json_decode((string)$responsefac->getBody());
			
			$responseacad = $client->request('get', 'get-acadprograme-information', [
                'json' => [ 'acadprogcode' => $acadprogcode ]
            ]);
            $acadprog = json_decode((string)$responseacad->getBody());
			
			$responsebatch = $client->request('get', 'get-batch-information', [
                'json' => [ 'batchcode' => $batchcode ]
            ]);
		
            $batch = json_decode((string)$responsebatch->getBody());
			
			$responsesemdate = $client->request('get', 'get-semdate-information', [
                'json' => [ 'semcode' => $semcode ]
            ]);
		
            $semdate = json_decode((string)$responsesemdate->getBody());
			
		
		$defaultstud  = DB::table('defaulter_fees_stud')
                         ->where('regno', $regno)
						 ->where('status', 1)
                         ->first();
		
		$rollnoslip = $user;
		// Dev Mannan: QR Code implementation for roll number slip starts
		// $chartUrl_tr = "https://chart.googleapis.com/chart?chf=bg,s,FFFFFF00&chs=80x80&cht=qr&chl=https://sis.iiu.edu.pk/transcript/".base64_encode($regno);
		$chartUrl_tr = "https://image-charts.com/chart?chf=bg,s,FFFFFF00&chs=50x50&cht=qr&chl=https://sis.iiu.edu.pk/transcript/".base64_encode($regno);
		$qrCodeImage_tr = file_get_contents($chartUrl_tr);
		$base64Image_tr = base64_encode($qrCodeImage_tr);
		// Dev Mannan: QR Code iimplementation for roll number slip ends
		// return view('exam.download-rollnoslip', compact('user', 'rollnoslip', 'picture', 'faculty', 'acadprog', 'batch', 'semdate', 'defaultstud'));
        $pdf = PDF::loadView('exam.download-rollnoslip', compact('user', 'rollnoslip', 'picture', 'faculty', 'acadprog', 'batch', 'semdate', 'defaultstud', 'base64Image_tr'));
		return $pdf->setPaper('a4', 'portrait')->stream();
    }

    public function emailrollnoslip(){
		
		$user = auth()->user();
		if($user->email == 'muhammad.talha@iiu.edu.pk')
		{
		  
		  return view('exam.rollnoslip-email');
		}
		else{
			  return redirect()->route('dashboard');
		}
		
	}
	
	public function Sentemailrollnoslip(){
		
		$user = auth()->user();
		$email = $user->email;
		
		if($user->email == 'muhammad.talha@iiu.edu.pk')
		{
			/**  Start Course Register and Course Register Email user **/
			
			$client = new Client([                                                // Live Setting
            'base_uri' => 'http://usis.iiu.edu.pk:64453/fees/'
            ]);
			
			$response = $client->request('get', 'get-CourseRegisteredStudent-information', [
                'json' => [ 'email' => $email ]
            ]);
			
			$userInfos = json_decode((string)$response->getBody());                // Live Setting
			
			//dd($userInfos);   // Not open just for testing
			
		/**  End Course Register and Course Register Email user **/
			
			//$regnos = array('3307-FBAS/BSSE/F16', '3309-FBAS/BSSE/F16');  // Not open just for testing
			
			//$toe = array('muhammad.talha@iiu.edu.pk', 'junaid.azhar@iiu.edu.pk');  // Not open just for testing
			$count = 0;
			foreach($userInfos as $key=> $userinfo)             // Live Setting
			//foreach($regnos  as $key=> $regno)
			{	 					
				$regno = $userinfo->regno;                // Live Setting
				$toemail = $userinfo->studentiiuiemail;   // Live Setting
				//$studname = $userinfo->studname;   // Live Setting
				//$toemail = $toe[$key];   // Not open just for testing
				$slip = $this->EmailRollNoSlipPdfAll($regno);  
				
				  try {
						$this->sendEmail($slip, $toemail);  //$studname add in bracket 
					} catch (Exception $e) {
						$exception = 'Email not sent';
						info($e);
						info($exception);
						return $exception;
						//send email to 'muhammad.talha@iiu.edu.pk' about mail exception at time
					}
				  
				 $count = $count+1; 
		     }
			 return view('exam.rollnoslip-email', compact('count'));
		}
		else{
			  return redirect()->route('dashboard');
		}
		
	}
	
	 public function sendEmail($filePath, $toemail)  //$studname add in bracket
    {		
		$user = auth()->user();
		$name = $user->name;
		//$studentname = $studname;
		$email_host = 'smtp-relay.gmail.com';
        $email_port = '587';
        $email_encryption = 'tls';
        $email_username = 'student.notifications@iiu.edu.pk';
        $email_password = '42369258';

        Config::set('mail.driver', 'smtp');
        Config::set('mail.host', $email_host);
        Config::set('mail.port', $email_port);
        Config::set('mail.encryption', $email_encryption);
        Config::set('mail.username', $email_username);
        Config::set('mail.password', $email_password);

        $to = $toemail;
        $subject = 'IIUI Exam Roll No Slip';
        Mail::send([], [], function ($message) use ($to, $subject, $email_username, $filePath) {
            $message->to($to)
                ->cc(['examination@iiu.edu.pk'])
                ->subject($subject)
				->setBody('<h3>Dear Student!</h3><br> 
				<P> Please find attached the provisional Roll No. Slip for terminal examination. Kindly note that Roll No. Slip does not confer any right to claim results of the semester, unless you clear your fee default.<p>
				<p>If you have any query related to Roll No Slip, than please create a ticket on Support System at <a href="https://support.iiu.edu.pk"> support.iiu.edu.pk</p>
				<br><br>
				<div style="background-color:lightgreen;">
				<b>Important:</b><br>
				<small>This is system generated email and can not accept replies.<br>
				</a> </small>
				</div>', 'text/html'); //$studname add in bracket
            $message->from($email_username, 'Student Notification');
            $message->attachData($filePath, "IIUI_Exam_RollNo_Slip.pdf");
        });
    }
	
	/**
    * Build pdf for a Roll No Slip
    */
    public function EmailRollNoSlipPdfAll($regno)
    {
		$user = json_decode($this->getRollNoSlip($regno));
		$faccode = $user[0]->faccode;
		$acadprogcode = $user[0]->acadprogcode;
		$batchcode = $user[0]->batchcode;
		$semcode = $user[0]->semcode;
		
		$client = new Client([
            'base_uri' => 'http://usis.iiu.edu.pk:64453/fees/'
            ]);
            $response = $client->request('get', 'get-picture-information', [
                'json' => [ 'regno' => $regno ]
            ]);
            $picture = (string)$response->getBody();
			
			$responsefac = $client->request('get', 'get-faculty-information', [
                'json' => [ 'faccode' => $faccode ]
            ]);
            $faculty = json_decode((string)$responsefac->getBody());
			
			$responseacad = $client->request('get', 'get-acadprograme-information', [
                'json' => [ 'acadprogcode' => $acadprogcode ]
            ]);
            $acadprog = json_decode((string)$responseacad->getBody());
			
			$responsebatch = $client->request('get', 'get-batch-information', [
                'json' => [ 'batchcode' => $batchcode ]
            ]);
		
            $batch = json_decode((string)$responsebatch->getBody());
			
			$responsesemdate = $client->request('get', 'get-semdate-information', [
                'json' => [ 'semcode' => $semcode ]
            ]);
		
            $semdate = json_decode((string)$responsesemdate->getBody());
		
		$rollnoslip = $user;
        $pdf = PDF::loadView('exam.download-rollnoslip', compact('user', 'rollnoslip', 'picture', 'faculty', 'acadprog', 'batch', 'semdate'));
        return $pdf->setPaper('a4', 'portrait')->stream();
    }
	public function rollnoslips()
    {
		return view('feechallanall');
	}
	
	public function getslipall(Request $request)
    {		
		$regno = $request->regno;
		
		$defaultstud  = DB::table('defaulter_fees_stud')
                         ->where('regno', $regno)
						 ->where('status', 1)
                         ->first();
		/*if(!empty($defaultstud) && $defaultstud->regno == $regno && $defaultstud->status == 1)
		   {
			   echo 'more information Contact to Fee Section.';
			   exit();
		   }*/
		
		$regno = $request->regno;
		$user = json_decode($this->getRollNoSlip($regno));
		if(empty($user))
		 {
			 echo 'No Course Registered Against Regno';
			 exit();
		 }
		if(empty($user[0]->faccode))
		 {
		   echo 'please try again later';
		  return redirect('/dashboard');
		 }
		$faccode = $user[0]->faccode;
		$acadprogcode = $user[0]->acadprogcode;
		$batchcode = $user[0]->batchcode;
		$semcode = $user[0]->semcode;
	    
		$client = new Client([
            'base_uri' => 'http://usis.iiu.edu.pk:64453/fees/'
            ]);
            $response = $client->request('get', 'get-picture-information', [
                'json' => [ 'regno' => $regno ]
            ]);
            $picture = (string)$response->getBody();
			
			$responsefac = $client->request('get', 'get-faculty-information', [
                'json' => [ 'faccode' => $faccode ]
            ]);
            $faculty = json_decode((string)$responsefac->getBody());
			
			$responseacad = $client->request('get', 'get-acadprograme-information', [
                'json' => [ 'acadprogcode' => $acadprogcode ]
            ]);
            $acadprog = json_decode((string)$responseacad->getBody());
			
			$responsebatch = $client->request('get', 'get-batch-information', [
                'json' => [ 'batchcode' => $batchcode ]
            ]);
		
            $batch = json_decode((string)$responsebatch->getBody());
			
			$responsesemdate = $client->request('get', 'get-semdate-information', [
                'json' => [ 'semcode' => $semcode ]
            ]);
		
            $semdate = json_decode((string)$responsesemdate->getBody());
		
		$rollnoslip = $user;
		
        return view('exam.rollno-slip', compact('user', 'rollnoslip', 'picture', 'faculty', 'acadprog', 'batch', 'semdate', 'defaultstud'));
    }
}
