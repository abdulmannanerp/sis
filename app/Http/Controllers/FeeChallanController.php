<?php

namespace App\Http\Controllers;

use App\Models\FineAdjustmentLog;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use PDF;

class FeeChallanController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $challan = json_decode($this->getFeeChallan($user->regno));
        return view('student.fee-challans', compact('user', 'challan'));
    }

    /**
     * Request aljmaia for a challan
     */
    public function getFeeChallan($regno)
    {
        $challanDetails = '';
        try {
            $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
            ]);
            $response = $client->request('POST', 'get-challan-information', [
                'json' => [ 'regno' => $regno ]
            ]);
            $challanDetails = (string)$response->getBody();
            // dd($challanDetails);
        } catch (RequestException $e) {
            info($e);
            $challanDetails = '';
        }
        return $challanDetails;
    }

    /**
    * Build pdf for a challan
    */
    public function exportPdf($id = null, $adminProvidedRegNo = null, $adjustFine = null, $fineAmount= null)
    {
        $regNo = auth()->user()->regno;
        if ($adminProvidedRegNo) {
            $regNo = $adminProvidedRegNo;
        }
        $challanDetails = json_decode($this->getFeeChallan($regNo));
        $copies = ['Bank Copy', 'Account Copy', 'Academic Copy', 'Student Copy'];
        // return view('student.download-challan', compact('challanDetails', 'copies'));
        $pdf = PDF::loadView('student.download-challan', compact('challanDetails', 'copies', 'adjustFine', 'fineAmount'));
        return $pdf->setPaper('a4', 'landscape')->stream();
    }

    public function adminPrintChallan()
    {
        if (strtolower(request()->method()) == 'post') {
            $regno = request('regno');
            $adjustFine = request('adjust-fine');
            $fineAmount = request('fine');
            if ($fineAmount) {
                FineAdjustmentLog::create([
                    'user_id' => auth()->id(),
                    'regno' => $regno,
                    'fine' => $fineAmount
                ]);
            }
            if ($regno) {
                return $this->exportPdf(null, $regno, $adjustFine, $fineAmount);
            }
        }
        return view('admin.print-challan');
    }
    /**
     * Download challan, Temp solution
     */
    public function exportPdfAdmin()
    {
        $regNo = request('regno');
        $challanDetails = json_decode($this->getFeeChallan($regNo));
        $copies = ['Bank Copy', 'Account Copy', 'Academic Copy', 'Student Copy'];
        $pdf = PDF::loadView('student.download-challan', compact('challanDetails', 'copies'));
        return $pdf->setPaper('a4', 'landscape')->stream();
    }
}
