<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;

class AuthenticationController extends Controller
{
    /**
     * If user is authenticated redirect to dashboard, otherwise send
     * user to sso
     */
    public function index()
    {
		$error = request()->session()->get('error');
        if (auth()->user()) {
            return redirect()->route('dashboard');
        }
        return view('auth.login', [
            'error' => $error
        ]);
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('google')->stateless()->user();
        } catch (\Exception $e) {
            return redirect()->route('login');
        }
        
        /**
         * Only allow users from IIU domain
         */
        if (explode("@", $user->email)[1] !== 'iiu.edu.pk' && explode("@", $user->email)[1] !== 'student.iiu.edu.pk') {
            return redirect()->to('/login')->with('error', 'Please user your IIU account to login');
        }

        /**
         * If existing user
         */
		 // $gcregno = $user->user['family_name'];
		 
        $userToAuthenticate = User::where('email', $user->email)->first();

        if (! $userToAuthenticate) {
            $aljamiaUser = $this->getUserDetailsFromAljamia($user->email);
            // $aljamiaUser = $this->getUserDetailsFromAljamia($user->email, $gcregno);
            if (!$aljamiaUser) {
                return redirect()
                    ->route('login')
                    ->with('error', 'All those students who got admission in Spring 2024 can download their challans from https://cms.iiu.edu.pk/. Profile update required for the previously enrolled students, and they need to contact admission office to update your CNIC & IIU Email');
            }

            $userToAuthenticate                  = new User;
            $userToAuthenticate->role_id         = 2;
            $userToAuthenticate->name            = $user->name;
            $userToAuthenticate->email           = $user->email;
            $userToAuthenticate->regno           = $aljamiaUser->regno;
            $userToAuthenticate->gender          = ($aljamiaUser->sex == 'M') ? 'Male' : 'Female';
            $userToAuthenticate->google_id       = $user->id;
            $userToAuthenticate->avatar          = $user->avatar;
            $userToAuthenticate->save();
        }
        auth()->login($userToAuthenticate);
        return redirect()->route('dashboard');
    }

    public function getUserDetailsFromAljamia($email)
    {
        $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);
        $request = $client->request('POST', 'get-student-by-email', [
            'json' => ['email' => $email]  
        ]);
        return json_decode((string)$request->getBody());
    }

    public function loginWithCnic()
    {
        $response = $error = '';
        if ( strtolower(request()->method()) == 'post') {
            $response = $this->postAuthWithCnic(request()->all());
            if ($response) {
                return $this->createUserAndAuthenticate($response);    
            }
            $error = 'Unable to find with provided details - Contact Admission Office for Correction in Your Data (Name & CNIC)';
        }
        return view('auth.login-with-cnic', compact('error'));
    }

    public function createUserAndAuthenticate($response)
    {
        $user = User::query()
            ->tap(function($query) use ($response) {
                if ($response->studentiiuiemail) {
                    $query = $query->where('email', $response->studentiiuiemail);
                }
                if ($response->studemail) {
                    $query = $query->orWhere('email', $response->studemail);
                }
                if ($response->nidpassno) {
                    $query = $query->orWhere('cnic', $response->nidpassno);
                }
                return $query;
            })->first();
        if (! $user) {
            $user = new User;
            $user->role_id = 2;
            $user->name = $response->studname;
            $user->email = $response->studentiiuiemail ?: $response->studemail;
            $user->regno = $response->regno;
            $user->cnic = $response->nidpassno;
            $user->gender = ($response->sex == 'M') ? 'Male' : 'Female';
            $user->save();
        }
        auth()->login($user);
        return redirect()->route('dashboard');
    }

    public function postAuthWithCnic($request)
    {
        $validator = Validator::make($request, [
            'name' => ['required'],
            'cnic' => ['required']
        ]);
        if ($validator->fails()) {
            return redirect()->route('loginWithCnic')
                ->withErrors($validator)
                ->withInput();
        }

        $name = ucwords(strtolower($request['name']));
        $cnic = $request['cnic'];
        $cnicWithoutDashes = str_replace('-', '', $request['cnic']);
        $client = new Client([
            'base_uri' => 'http://111.68.97.170:64464/'
        ]);
        $request = $client->request('POST', 'get-student-by-name-and-cnic', [
            'json' => [
                'name' => $name,
                'cnic' => $cnic,
                'cnicWithoutDashes' => $cnicWithoutDashes
            ]  
        ]);
        // dd((string)$request->getBody());
        // exit();
        return json_decode((string)$request->getBody());
    }

    /**
     * Logout user
     */
    public function logout()
    {
        auth()->logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect('/');
    }
}
