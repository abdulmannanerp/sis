<?php

namespace App\Http\Controllers;

use App\Models\Batch;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function getBatches()
    {
    	$content = json_decode(request()->getContent());
    	if (! $content) {
    		return;
    	}
    	return Batch::where('acadprogcode', $content->program)->get();
    }
}
