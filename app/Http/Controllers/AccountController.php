<?php

namespace App\Http\Controllers;

use App\Mail\PasswordUpdated;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AccountController extends Controller
{
    /**
     * Return change password view
     */
    public function changePassword()
    {
        $user = auth()->user();
        $status = request()->session()->get('status');
        return view('student.change-password', compact('user', 'status'));
    }

    /**
     * Submit request to SSO to change user's password
     */
    public function updatePassword()
    {
        request()->validate([
            'password' => 'required|confirmed'
        ]);
        $client = new Client([
            'base_uri' => 'https://sso.iiu.edu.pk'
        ]);
        $request = $client->request('POST', '/change-user-password', [
            'json' => [
                'userId' => request('ssoUserId'),
                'token' => request('ssoAuthToken'),
                'password' => request('password')
            ]
        ]);
        $response = json_decode((string)$request->getBody());
        if ($response->status == 'success') {
            $user = auth()->user();
            Mail::to($user->email)->cc('fee.challan@iiu.edu.pk')->send(
                new PasswordUpdated($user, request('password'))
            );
            return back()->with('status', 'Password updated successfully');
        }
        return back()->withErrors('Password Updated Failed, Please try again later');
    }
}
